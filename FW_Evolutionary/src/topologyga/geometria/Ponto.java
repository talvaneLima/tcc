/*
 * Copyright (C) 2014 Manoel Afonso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package topologyga.geometria;

/**
 * Representa um ponto (x, y) no plano cartesiano.
 *
 * @author Manoel Afonso
 */
public class Ponto {

    /**
     * Coordenada x.
     */
    private int x;

    /**
     * Coordenada y.
     */
    private int y;

    /**
     * Cria um ponto com a coordenada (0, 0).
     */
    public Ponto() {
        this.x = 0;
        this.y = 0;
    }

    /**
     * Cria um ponto com as coordenadas especificadas.
     *
     * @param x Valor x.
     * @param y Valor y.
     */
    public Ponto(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * @return Coordenada x.
     */
    public int getX() {
        return x;
    }

    /**
     * @param x Coordenada x.
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * @return Coordenada y.
     */
    public int getY() {
        return y;
    }

    /**
     * @param y Coordenada y.
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * @return String no formato (x, y).
     */
    @Override
    public String toString() {
        return "(" + this.x + ", " + this.y + ")";
    }

}
