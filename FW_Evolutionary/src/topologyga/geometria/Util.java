/*
 * Copyright (C) 2014 Manoel Afonso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package topologyga.geometria;

import java.util.ArrayList;

/**
 * Classe para computar cálculos geométricos.
 *
 * @author Manoel Afonso
 */
public class Util {

    /**
     * Calcula a distância euclidiana entre os pontos P1 e P2.
     *
     * @param p1 Objeto ponto P1.
     * @param p2 Objeto ponto P2.
     * @return A distância euclidiana entre os pontos P1 e P2.
     */
    public static double distanciaEuclidiana(Ponto p1, Ponto p2) {
        return distanciaEuclidiana(p1.getX(), p1.getY(), p2.getX(), p2.getY());
    }

    /**
     * Calcula a distância euclidiana entre os pontos P1(x1, y1) e P2(x2, y2).
     *
     * @param x1 Coordenada x de P1.
     * @param y1 Coordenada y de P1.
     * @param x2 Coordenada x de P2.
     * @param y2 Coordenada y de P2.
     * @return A distância euclidiana entre os pontos P1 e P2.
     */
    public static double distanciaEuclidiana(int x1, int y1, int x2, int y2) {
        return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
    }

    /**
     * Verifica se o ponto p está dentro do círculo de centro
     * <code>centro</code> e raio <code>raio</code>.
     *
     * @param p Ponto a ser verificado.
     * @param centro Centro do círculo.
     * @param raio Raio do círculo.
     * @return true se o ponto p está contido no círculo, false caso contrário.
     */
    public static boolean estaNoCirculo(Ponto p, Ponto centro, double raio) {
        return estaNoCirculo(p.getX(), p.getY(), centro.getX(), centro.getY(), raio);
    }

    /**
     * Verifica se o ponto (x, y) está dentro do círculo de centro (circ_x,
     * circ_y) e raio <code>raio</code>. Um ponto no limite do círculo, é
     * considerado dentro do círculo.
     *
     * @param x Coordenada x do ponto.
     * @param y Coordenada y do ponto.
     * @param circ_x Coordenada x do centro do círculo.
     * @param circ_y Coordenada y do centro do círculo.
     * @param raio Raio do círculo.
     * @return true se o ponto está contido no círculo, false caso contrário.
     */
    public static boolean estaNoCirculo(int x, int y, int circ_x, int circ_y, double raio) {
        return Math.pow(x - circ_x, 2) + Math.pow(y - circ_y, 2) <= Math.pow(raio, 2);
    }

    /**
     * Verifica se as retas p1-q1 e p2-q2 cruzam. Nota: Se um extremo de uma
     * reta tocar algum ponto da outra, então há cruzamento. Fonte:
     * http://www.dcs.gla.ac.uk/~pat/52233/slides/Geometry1x1.pdf
     *
     * @param p1 Coordenada inicial da primeira reta.
     * @param q1 Coordenada final da primeira reta.
     * @param p2 Coordenada inicial da segunda reta.
     * @param q2 Coordenada final da segunda reta.
     * @return true se as retas se cruzam, false caso contrário.
     */
    public static boolean retasCruzam(Ponto p1, Ponto q1, Ponto p2, Ponto q2) {
        // Procura as quatro orientações para os casos gerais e especiais.
        int o1 = orientacao(p1, q1, p2);
        int o2 = orientacao(p1, q1, q2);
        int o3 = orientacao(p2, q2, p1);
        int o4 = orientacao(p2, q2, q1);

        // Caso geral
        if (o1 != o2 && o3 != o4) {
            return true;
        }

        // Casos especiais
        // p1, q1 e p2 são colineares e p2 está no segmento p1-q1.
        if (o1 == 0 && noSegmento(p1, p2, q1)) {
            return true;
        }

        // p1, q1 e p2 são colineares e q2 está no segmento p1-q1.
        if (o2 == 0 && noSegmento(p1, q2, q1)) {
            return true;
        }

        // p2, q2 e p1 são colineares e p1 está no segmento p2q2.
        if (o3 == 0 && noSegmento(p2, p1, q2)) {
            return true;
        }

        //p2, q2 e q1 são colineares e q1 está no segmento p2q2.
        //Se for false, então não caiu em nenhum caso e portanto não se cruzam.
        return o4 == 0 && noSegmento(p2, q1, q2);
    }

    /**
     * Verifica se a reta p1-q1 cruza o polígono pol. Cada aresta do polígono é
     * testada para saber se ela cruza com a reta p1-q1. Se houver um
     * cruzamento, então a reta cruza o polígono. Nota: Se um extremo da reta
     * tocar algum ponto de alguma aresta do polígono, então há cruzamento.
     *
     * @param p1 Coordenada inicial da reta.
     * @param q1 Coordenada final da reta.
     * @param pol Polígono
     * @return true se a reta p1-q1 cruza o polígono pol, false caso contrário.
     */
    public static boolean retaCruzaPoligono(Ponto p1, Ponto q1, Poligono pol) {
        // Pontos do polígono.
        ArrayList<Ponto> pontos = pol.getPontos();

        for (int i = 0; i < pontos.size(); i++) {
            // Verifica se há cruzamento entre a reta a aresta atual.
            // A aresta é definida pelo ponto atual com o seu adjacente.
            // No caso do último vértice, forma-se uma aresta com o primeiro.
            if (retasCruzam(p1, q1, pontos.get(i), pontos.get((i + 1) % pontos.size()))) {
                return true;
            }
        }

        // A reta p1-q1 não cruzou nenhuma aresta do polígono.
        return false;
    }

    /**
     * Dado três pontos colineares p, q e r, verifica-se se o ponto q está no
     * segmento de reta p-r.
     *
     * @param p Ponto p.
     * @param q Ponto q.
     * @param r Ponto r.
     * @return true se o ponto q está na reta p-r, false caso contrário.
     */
    private static boolean noSegmento(Ponto p, Ponto q, Ponto r) {
        return q.getX() <= Math.max(p.getX(), r.getX())
                && q.getX() >= Math.min(p.getX(), r.getX())
                && q.getY() <= Math.max(p.getY(), r.getY())
                && q.getY() >= Math.min(p.getY(), r.getY());
    }

    /**
     * Encontra a orientação de uma tupla ordenada (p, q, r).
     *
     * @param p Ponto p.
     * @param q Ponto q.
     * @param r Ponto r.
     * @return 0 se os pontos p, q e r são colineares. 1 para o sentido horário.
     * 2 para o sentido anti-horário.
     */
    private static int orientacao(Ponto p, Ponto q, Ponto r) {
        int val = (q.getY() - p.getY()) * (r.getX() - q.getX())
                - (q.getX() - p.getX()) * (r.getY() - q.getY());

        if (val == 0) {
            return 0;  // colineares
        }
        return (val > 0) ? 1 : 2; // horário ou anti-horário
    }

}
