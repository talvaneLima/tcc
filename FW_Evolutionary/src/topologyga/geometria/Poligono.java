/*
 * Copyright (C) 2014 Manoel Afonso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package topologyga.geometria;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Classe representando um polígono como um conjunto de pontos.
 *
 * @author Manoel Afonso
 */
public class Poligono {

    /**
     * Pontos do polígono.
     */
    private ArrayList<Ponto> pontos;

    /**
     * ID deste polígono. Deve ser um número inteiro positivo e único entre
     * todas as ID's.
     */
    private int id;

    /**
     * Cria um polígono com o nome dado, mas sem ponto algum.
     *
     * @param id ID do polígono.
     */
    public Poligono(int id) {
        this.id = id;
        this.pontos = new ArrayList<>();
    }

    /**
     * Cria um polígono com um nome e seus respectivos pontos.
     *
     * @param id ID do polígono.
     * @param pontos Conjunto de pontos.
     */
    public Poligono(int id, Ponto... pontos) {
        this.id = id;
        this.pontos = new ArrayList<>(pontos.length);
        this.pontos.addAll(Arrays.asList(pontos));
    }

    /**
     * Cria um polígono com os seus pontos.
     *
     * @param pontos
     */
    public Poligono(Ponto... pontos) {
        this.pontos = new ArrayList<>(pontos.length);
        this.pontos.addAll(Arrays.asList(pontos));
    }

    /**
     * Adiciona um ponto ao polígono.
     *
     * @param ponto Novo ponto.
     */
    public void adicionarPonto(Ponto ponto) {
        this.pontos.add(ponto);
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return O conjunto de pontos deste polígono.
     */
    public ArrayList<Ponto> getPontos() {
        return pontos;
    }

    /**
     * @param pontos O novo conjunto de pontos.
     */
    public void setPontos(ArrayList<Ponto> pontos) {
        this.pontos = pontos;
    }

    /**
     * @return Uma representação do polígono no formato [ID][Pontos...]
     */
    @Override
    public String toString() {
        String s = "ID: " + this.id + ", Pontos: ";
        for (int i = 0; i < this.pontos.size(); i++) {
            s += "P" + this.pontos.get(i);
            if (i < this.pontos.size() - 1) {
                s += ", ";
            }
        }
        return s;
    }

}
