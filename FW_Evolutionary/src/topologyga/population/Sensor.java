/*
 * Copyright (C) 2014 Manoel Afonso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package topologyga.population;

import java.util.ArrayList;
import java.util.Objects;
import topologyga.geometria.Ponto;

/**
 * Classe para representar um sensor. Um sensor compreende a um ponto
 * (coordenada) no plano bem como o seu respectivo comprimento de onda e
 * identificador.
 *
 * @author Manoel Afonso
 */
public class Sensor {

    /**
     * Identificador deste sensor. Deve ser um inteiro positivo e único entre
     * todos os sensores.
     */
    private int id;
    
    /**
     * Identificador do interrogador. Por padrão é -1.
     */
    public static final int ID_INTERROGADOR = -1;
    
    /**
     * Comprimento de onda lambda do interrogador. Por padrão é -1 para não
     * causar conflito com os lambdas dos sensores.
     */
    public static final double LAMBDA_INTERROGADOR = -1;

    /**
     * Se true, indica que este sensor já foi instalado em algum canal.
     */
    private boolean instalado;

    /**
     * Comprimento de onda deste sensor. Deve ser sempre positivo.
     */
    private double lambda;

    /**
     * Ponto no plano onde este sensor está localizado.
     */
    private Ponto local;

    /**
     * Lista de sensores adjacentes (vizinhos) a este sensor.
     */
    private ArrayList<Sensor> adjacentes;

    /**
     * Cria um novo sensor.
     *
     * @param id Identificador do sensor.
     * @param local Ponto no plano no qual este sensor está.
     * @param lambda Comprimento de onda.
     */
    public Sensor(int id, Ponto local, double lambda) {
        this.id = id;
        this.local = local;
        this.lambda = lambda;
        this.adjacentes = new ArrayList<>();
    }

    /**
     * Calcula o índice do próximo do sensor dado o gene. O cálculo é feito
     * por:<br> {@code i = floor ( g / ( 1 / tot ) )}<br>
     * onde {@code i} é o índice da lista de adjacências, {@code g} é o valor do
     * gene e {@code tot} é o tamanho da lista. Para uma lista de 3 sensores
     * temos os seguintes limites:<br>
     * <table>
     * <tr><td>Índice</td><td>Intervalo</td></tr>
     * <tr><td>0</td><td>0.00 - 0.33</td></tr>
     * <tr><td>1</td><td>0.34 - 0.66</td></tr>
     * <tr><td>2</td><td>0.67 - 0.99</td></tr>
     * </table>
     *
     * @param gene Valor do gene, no intervalo [0,1], que indica o próximo
     * sensor.
     * @return O índice na lista de adjacência deste sensor.
     * @see Sensor#indiceParaGene(int) Limite superior de um intervalo de um
     * índice.
     */
    public int geneParaIndice(double gene) {
        int indice = (int) Math.floor(gene / (1.0 / this.adjacentes.size()));
        return indice;
    }

    /**
     * Calcula o limite superior de um intervalo de um índice específico. O
     * cálculo é feito por:<br> {@code sup = ( i + 1 ) * ( 1 / tot )}<br>
     * onde {@code i} é o índice da lista de adjacências e {@code tot} é o
     * tamanho da lista. Para os intervalos abaixo, sup(0) = 0.33, sup(1) = 0.66
     * e sup(2) = 1.<br>
     * <table>
     * <tr><td>Índice</td><td>Intervalo</td></tr>
     * <tr><td>0</td><td>0.00 - 0.33</td></tr>
     * <tr><td>1</td><td>0.34 - 0.66</td></tr>
     * <tr><td>2</td><td>0.67 - 0.99</td></tr>
     * </table>
     *
     * @param ind Índice cujo limite superior será calculado.
     * @return Limite superior de {@code ind}.
     */
    public double indiceParaGene(int ind) {
        //FIXME Encontrar um fórmula melhor para calcular o limite superior do
        //intervalo de uma posição da lista. Subtrair 0.001 não é adequado, mas
        //pelo menos vai funcionar com até 1000 sensores :)
        return (ind * (1.0 / this.adjacentes.size()) + (1.0 / this.adjacentes.size())) - 0.001;
    }

    /**
     * Adiciona um sensor à lista de adjacências deste sensor.
     *
     * @param s Sensor vizinho a este.
     */
    public void addAdjacente(Sensor s) {
        this.adjacentes.add(s);
    }

    /**
     * Verifica se o sensor s é adjacente a este sensor;
     *
     * @param s Sensor a ser verificado.
     * @return true se s é adjacente, false caso contrário.
     */
    public boolean isAdjacente(Sensor s) {
        return this.adjacentes.contains(s);
    }

    /**
     * Verifica se este sensor é um interrogador. A id do interrogador é sempre
     * "*".
     *
     * @return true se este sensor está representando o interrogador, false caso
     * contrário.
     */
    public boolean isInterrogador() {
        return this.getId() == ID_INTERROGADOR;
    }
    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the lambda
     */
    public double getLambda() {
        return lambda;
    }

    /**
     * @param lambda the lambda to set
     */
    public void setLambda(double lambda) {
        this.lambda = lambda;
    }

    /**
     * @return the local
     */
    public Ponto getLocal() {
        return local;
    }

    /**
     * @param local the local to set
     */
    public void setLocal(Ponto local) {
        this.local = local;
    }

    /**
     * @return A lista de adjacência deste sensor.
     */
    public ArrayList<Sensor> getAdjacentes() {
        return adjacentes;
    }

    /**
     * @return Uma string formatada com os adjacentes deste sensor com o
     * seguinte padrão: [id_adjacente], [id_adjacente], ...
     */
    public String getStringAdjacentes() {
        String s = "";
        for (Sensor sensor : adjacentes) {
            s += sensor.getId() + " ";
        }
        return s;
    }

    /**
     * @param adjacentes the adjacentes to set
     */
    public void setAdjacentes(ArrayList<Sensor> adjacentes) {
        this.adjacentes = adjacentes;
    }

    /**
     * Dois sensores são iguais se ambos possuírem a mesma ID.
     *
     * @param obj Sensor a ser comparado.
     * @return true se os sensores são iguais, false caso contrário.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Sensor) {
            Sensor s = (Sensor) obj;
            return this.id == s.getId();
        } else {
            throw new IllegalArgumentException("obj não é do tipo sensor.");
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.getId());
        hash = 59 * hash + (int) (Double.doubleToLongBits(this.lambda) ^ (Double.doubleToLongBits(this.lambda) >>> 32));
        hash = 59 * hash + Objects.hashCode(this.local);
        hash = 59 * hash + Objects.hashCode(this.adjacentes);
        return hash;
    }

    /**
     * @return Representação deste indivíduo no formato
     * [id][lambda][localização][fitness].
     */
    @Override
    public String toString() {
        return "ID: " + this.getId() + ", P" + this.local + ", λ: " + this.lambda;
    }

    /**
     * @return the instalado
     */
    public boolean isInstalado() {
        return instalado;
    }

    /**
     * Marca o sensor como instalado em alguma fibra.
     *
     * @param instalado the instalado to set
     */
    public void setInstalado(boolean instalado) {
        this.instalado = instalado;
    }

}
