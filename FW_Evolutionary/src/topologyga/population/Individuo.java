/*
 * Copyright (C) 2014 Manoel Afonso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package topologyga.population;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import topologyga.context.AG;
import topologyga.printer.DefaultPrinter;
import topologyga.printer.Printer;

/**
 * <p>
 * Modelagem do indivíduo representando um possível caminho na topologia. Ele é
 * constituído de um conjunto de fibras, as quais podem ou não possuir sensores
 * conectados. Assim, cada indivíduo tem a quantidade máxima de fibras
 * disponíveis, porém nem todas estão necessariamente usadas. Um caminho leva em
 * consideração a quantidade de sensores e a quantidade de canais disponíveis.
 * </p>
 * <p>
 * O cromossomo do indivíduo consiste na concatenação dos "cromossomos" de cada
 * canal. Exemplo de indivíduo de uma topologia com 2 fibras disponíveis, cada
 * uma com 5 espaços disponíveis para sensores:
 * <p>
 * <code>
 * [0.5][0.2][0.4][0.2][-0.5][0.2][-0.5][-0.5][-0.5][-0.5]
 * </code>
 * <p>
 * Assim é possível saber que os sensores da primeira fibra estão nas posições 0
 * a 4 e os sensores da segunda nas posições 5 a 9. Valores no intervalo [0,1)
 * indicam o próximo sensor, e um valor negativo indica uma posição da fibra não
 * utilizada.
 * </p>
 *
 * @author Manoel Afonso
 */
public class Individuo implements Comparable<Individuo> {
    
    /**
     * Gene para o interrogador. Por padrão é -1.
     */
    public static final double GENE_INTERROGADOR = -1;
    
    /**
     * Valor dos genes inativos após o dead-end. Por padrão é -0.5.
     */
    public static final double GENE_INATIVO = -0.5;
    
    /**
     * Índice da frente de pareto. Note que o valor mínimo é 1.
     */
    private int paretoFront = 1;
    
    /**
     * Distância de multidão.
     */
    private double crowdingDistance = 0;
    
    /**
     * Array com os objetivos deste indivíduo. Útil para versões
     * multiobjetivas do AG.
     */
    private double objetivos[];
    
    /**
     * Valor do fitness deste indivíduo.
     */
    private double fitness;

    /**
     * Cromossomo do indivíduo. Concatenação dos arrays de cada fibra. Um valor
     * no intervalo [0,1) indica o próximo sensor da fibra. Um valor negativo
     * indica posição não utilizada.
     */
    private final double[] cromossomo;

    /**
     * Conjunto de objetos {@code Canal} presentes neste indivíduo. Eles são
     * úteis para o cálculo do fitness.
     */
    private Canal[] canais;

    /**
     * Contador para a quantidade de canais atual.
     */
    private int canaisAdicionados;

    /**
     * Quantidade de canais disponíveis no interrogador.
     */
    private final int numCanais;

    /**
     * Quantidade máxima de sensores por canal.
     */
    private final int numMaxSensor;

    /**
     * Comprimento total de fibra todos os canais presentes neste indivíduo.
     */
    private double comprimentoTotal;

    /**
     * Geração na qual este indivíduo foi criado.
     */
    private final int nasc;
    
    /**
     * Printer para o indivíduo.
     */
    public static Printer printer = new DefaultPrinter();

    /**
     * Cria um novo indivíduo.
     *
     * @param numCanais Número de canais disponíveis no interrogador.
     * @param numMaxSensor Número máximo de sensores por canal.
     */
    public Individuo(int numCanais, int numMaxSensor) {
        //Aloca espaço para todos os canais e sensores.
        this.cromossomo = new double[numCanais * numMaxSensor];
        //Por padrão, todos os genes estão desativados.
        for (int i = 0; i < this.cromossomo.length; i++) {
            this.cromossomo[i] = GENE_INATIVO;
        }
        //Inicias os outros atributos do indivíduo.
        this.numCanais = numCanais;
        this.numMaxSensor = numMaxSensor;
        this.canais = new Canal[numCanais];
        this.canaisAdicionados = 0;
        this.fitness = 0;
        //Grava a geração que este indivíduo está sendo criado.
        this.nasc = AG.getGeracaoAtual();
    }

    /**
     * Adiciona um conjunto de canais a este indivíduo.
     *
     * @param canais Array com os canais.
     */
    public void adicionarCanal(Canal[] canais) {
        for (Canal c : canais) {
            this.adicionarCanal(c);
        }
    }

    /**
     * Adiciona um canal ao cromossomo deste indivíduo. Após a inserção do
     * canal, o {@link Individuo#comprimentoTotal comprimento total} é
     * incrementado com o comprimento do canal {@code c}.
     *
     * @param c Canal a ser adicionado.
     */
    public void adicionarCanal(Canal c) {
        //Não pode extrapolar o número máximo de canais.
        if (this.canaisAdicionados <= this.numCanais) {
            //Posição a partir da qual começa o cromossomo do canal.
            int inicio = this.numMaxSensor * this.canaisAdicionados;
            //Copia os genes para este indivíduo.
            double[] genes = c.getCromossomo();
            System.arraycopy(genes, 0, this.getCromossomo(), inicio, genes.length);

            //Calcula o comprimento do canal
            this.comprimentoTotal += c.calcularComprimento();

            //Guarda o objeto Canal
            this.canais[this.canaisAdicionados] = c;

            //Incrementa a quantidade de canais presentes.
            this.canaisAdicionados++;
        }
    }
    
    /**
     * Calcula a quantidade de lambdas distintos presentes nos canais
     * deste indivíduo.
     * @return A quantidade de lambdas distintos.
     */
    public int qtdeLambdasDistintos(){
        ArrayList<Double> lambdas = new ArrayList<>();
        for (Canal c : this.canais) {
            if (c != null) {
                Sensor[] sensores = c.getSensores();
                for (Sensor sensor : sensores) {
                    if(sensor != null && !sensor.isInterrogador()){
                        double l = sensor.getLambda();
                        if(!lambdas.contains(l)){
                            lambdas.add(l);
                        }
                    }
                }
            }
        }
        return lambdas.size();
    }
    
    /**
     * Obtém o valor de um objetivo.
     * @param objetivo Índice do objetivo na lista de objetivos deste indivíduo.
     * @return O valor do objetivo especificado pelo índice {@code objetivo}.
     */
    public double getObjective(int objetivo){
        return this.getObjetivos()[objetivo];
    }

    /**
     * @return O valor do fitness.
     */
    public double getFitness() {
        return fitness;
    }

    /**
     * @param fitness Novo valor do fitness.
     */
    public void setFitness(double fitness) {
        this.fitness = fitness;
    }

    /**
     * @return O cromossomo deste indivíduo. Alterações no array serão
     * refletidas no array interno deste objeto.
     */
    public double[] getCromossomo() {
        return cromossomo;
    }

    /**
     * <b>Copia</b> o novo cromossomo para este indivíduo de forma que
     * alterações feitas no array <code>crom</code> <b>não</b> sejam refletidas
     * no array deste indivíduo.
     *
     * @param crom O novo cromossomo a ser inserido.
     */
    public void setCromossomo(double[] crom) {
        //Realiza a cópia
        System.arraycopy(crom, 0, this.cromossomo, 0, crom.length);
    }

    /**
     * Ordena em ordem crescente de acordo com o valor do fitness.
     *
     * @param o Individuo para ser comparado com este.
     * @return 1 se este indivíduo tiver fitness maior, 0 se os dois possuírem o
     * mesmo valor do fitness, e -1 caso o outro indivíduo tenha fitness maior.
     */
    @Override
    public int compareTo(Individuo o) {
        if (this.getFitness() > o.getFitness()) {
            return 1;
        } else if (this.getFitness() == o.getFitness()) {
            return 0;
        } else {
            return -1;
        }
    }

    /**
     * @return Uma string com os valores dos cromossomos no formato:
     * {@code x x x x}. Exemplo: {@code 0.2 0.32 0.44 0.1 0.2}.
     */
    public String getStringCromossomo() {
        NumberFormat numberFormat = new DecimalFormat("#.##");
        String s = "";
        for (int i = 0; i < this.getCromossomo().length; i++) {
            s += numberFormat.format(this.getCromossomo()[i]);
            if (i < this.getCromossomo().length - 1) {
                s += "\t";
            }
        }
        return s;
    }

    /**
     * @return A decodificação deste índivíduo, consistindo nas ID's dos
     * sensores que compõe todos os canais disponíveis.
     */
    public String getStringDecodificacao() {
        String s = "";
        for (Canal canal : canais) {
            s += canal.getDecodificacao() + " | ";
        }
        return s;
    }

    /**
     * @return the comprimentoTotal
     */
    public double getComprimentoTotal() {
        return comprimentoTotal;
    }

    /**
     * @param comprimentoTotal the comprimentoTotal to set
     */
    public void setComprimentoTotal(double comprimentoTotal) {
        this.comprimentoTotal = comprimentoTotal;
    }

    /**
     * @return the canais
     */
    public Canal[] getCanais() {
        return canais;
    }

    /**
     * @param canais the canais to set
     */
    public void setCanais(Canal[] canais) {
        this.canais = canais;
    }

    /**
     * @return the nasc
     */
    public int getNasc() {
        return nasc;
    }
    
    /**
     * @return the paretoFront
     */
    public int getParetoFront() {
        return paretoFront;
    }

    /**
     * @param paretoFront the paretoFront to set
     */
    public void setParetoFront(int paretoFront) {
        this.paretoFront = paretoFront;
    }
    
    /**
     * @return the crowdingDistance
     */
    public double getCrowdingDistance() {
        return crowdingDistance;
    }

    /**
     * @param crowdingDistance the crowdingDistance to set
     */
    public void setCrowdingDistance(double crowdingDistance) {
        this.crowdingDistance = crowdingDistance;
    }

    /**
     * Obtém uma string representando este indivíduo de acordo com o
     * {@link Individuo#printer printer} especificado.
     * @return Representação deste indivíduo.
     */
    @Override
    public String toString() {
        return Individuo.printer.print(this);
    }

    /**
     * Dois indivíduos são iguais se possuírem o mesmo fitness.
     *
     * @param obj Indivíduo a ser comparado.
     * @return true se {@code obj} tiver o mesmo fitness que este indivíduo,
     * false caso contrário.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Individuo) {
            Individuo ind = (Individuo) obj;
            return this.fitness == ind.getFitness();
        } else {
            throw new IllegalArgumentException("obj deve ser do tipo Individuo");
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + (int) (Double.doubleToLongBits(this.fitness) ^ (Double.doubleToLongBits(this.fitness) >>> 32));
        hash = 37 * hash + Arrays.hashCode(this.cromossomo);
        hash = 37 * hash + Arrays.deepHashCode(this.canais);
        hash = 37 * hash + this.canaisAdicionados;
        hash = 37 * hash + this.numCanais;
        hash = 37 * hash + this.numMaxSensor;
        hash = 37 * hash + (int) (Double.doubleToLongBits(this.comprimentoTotal) ^ (Double.doubleToLongBits(this.comprimentoTotal) >>> 32));
        hash = 37 * hash + this.nasc;
        return hash;
    }

    /**
     * @return the objetivos
     */
    public double[] getObjetivos() {
        return objetivos;
    }

    /**
     * @param objetivos the objetivos to set
     */
    public void setObjetivos(double[] objetivos) {
        this.objetivos = objetivos;
    }

}
