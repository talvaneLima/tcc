/*
 * Copyright (C) 2014 Manoel Afonso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package topologyga.population;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import topologyga.geometria.Util;

/**
 * Classe representando um canal. Cada canal possui um número máximo de sensores
 * instalados. Um canal é apenas uma parte do indivíduo, pois ele é constuído de
 * todos os canais, e estes, por sua vez, são constuídos pelos sensores.
 * O cromossomo é no formato [gene1] [gene2] [gene3] [gene4] [gene5] [gene6]...
 * Se o máximo de sensores por canal é três, então fica impĺícito que os canais
 * são:
 * 1º [gene1] [gene2] [gene3]
 * 2º [gene4] [gene5] [gene6]
 *
 * @author Manoel Afonso
 */
public class Canal {

    /**
     * Identificador deste canal.
     */
    private int id = 0;

    /**
     * Se true, indica que este canal ainda pode receber sensores, false caso
     * contrário. O critério que define se o canal está usado ou não é
     * arbitrário.
     */
    private boolean usado = false;

    /**
     * Conjunto de sensores. Servem para analizar os lambdas e a vizinhança.
     */
    private Sensor[] sensores;

    /**
     * Contador para garantir que não sejam inseridos mais sensores que o
     * máximo.
     */
    private int cont;

    /**
     * Representação cromossômica deste canal. Esta é a parte que estará no
     * indivíduo.
     */
    private double[] cromossomo;

    /**
     * Cria um canal.
     *
     * @param maxSensor Número máximo de sensores que podem haver neste canal.
     */
    public Canal(int maxSensor) {
        this.sensores = new Sensor[maxSensor];
        this.cromossomo = new double[maxSensor];
        //Preenche com -0.5 para indicar que todos os genes estão inativos.
        for (int i = 0; i < this.cromossomo.length; i++) {
            this.cromossomo[i] = Individuo.GENE_INATIVO;
        }
        this.cont = 0;
    }

    /**
     * Adiciona um sensor a este canal apenas se ainda estiver espaço.
     *
     * @param s Sensor a ser adicionado.
     * @param gene Valor do gene representando este sensor.
     * @return true se o sensor foi adicionado, false caso contrário.
     */
    public boolean addSensor(Sensor s, double gene) {
        if (this.haEspaco()) {
            this.sensores[this.cont] = s;
            this.cromossomo[this.cont] = gene;
            this.cont++;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Um canal está vazio quando não possui qualquer sensor instalado
     * nele. Isso é verificado se o primeiro gene do cromossomo é um valor
     * inativo. Se for, então o canal está vazio.
     *
     * @return true se este canal ainda não tem qualquer sensor instalado nele,
     * false caso contrário.
     */
    public boolean isVazio() {
        return this.cromossomo[0] == Individuo.GENE_INATIVO;
    }

    /**
     * Remove todos os sensores deste canal.
     */
    public void removerTodos() {
        this.sensores = new Sensor[cromossomo.length];
        this.cromossomo = new double[cromossomo.length];
        //Preenche com -0.5 para indicar que todos os genes estão inativos.
        for (int i = 0; i < this.cromossomo.length; i++) {
            this.cromossomo[i] = Individuo.GENE_INATIVO;
        }
    }

    /**
     * Verifica se há espaço no canal para a inserção de novos sensores.
     *
     * @return true se for possível inserir, false caso contrário.
     */
    public boolean haEspaco() {
        return this.cont < this.sensores.length;
    }

    /**
     * Verifica se o lambda dado já está no canal.
     *
     * @param lambda lambda a ser analisado.
     * @return true se o lambda já está presente, false caso contrário.
     */
    public boolean lambdaPresente(double lambda) {
        for (Sensor sensor : sensores) {
            if (sensor != null && sensor.getLambda() == lambda) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return O último sensor instalado neste canal ou null caso não haja
     * sensor algum instalado.
     */
    public Sensor getUltimoSensor() {
        for (int i = this.sensores.length - 1; i >= 0; i--) {
            if (this.sensores[i] != null) {
                return this.sensores[i];
            }
        }
        return null;
    }

    /**
     * Calcula o comprimento da fibra presente neste canal.
     *
     * @return O comprimento do canal.
     */
    public double calcularComprimento() {
        double comp = 0;
        //TODO criar um iterator?
        //Percorre os sensores.
        for (int i = 0; i < this.sensores.length; i++) {
            /*
             Como a distância é feita par a par, vamos até a penúltima posição e
             verificamos se há um próximo sensor para calcular a distância.
             */
            if (i < this.sensores.length - 1 && this.sensores[i + 1] != null) {
                //Obtém um par de sensores e calcula a distância do trecho.
                Sensor s1 = this.sensores[i];
                Sensor s2 = this.sensores[i + 1];
                comp += Util.distanciaEuclidiana(s1.getLocal(), s2.getLocal());
            }
        }
        return comp;
    }

    /**
     * @return the sensores
     */
    public Sensor[] getSensores() {
        return sensores;
    }

    /**
     * @param sensores the sensores to set
     */
    public void setSensores(Sensor[] sensores) {
        this.sensores = sensores;
    }

    /**
     * <p>
     * Obtém a representação cromossômica deste canal, no formato para ser
     * inserido em um indivíduo. O cromossomo de um canal consiste em um array
     * com as probabilidades de visita de cada sensor.
     * </p>
     * <code>
     * [gene1] [gene2] ... [gene_max]
     * </code>
     * <p>
     * <i>Dead-ends</i> são identificados com o valor -0.5. Exemplo de um canal
     * com no máximo 5 sensores e com dead-end.
     * </p>
     * <code>
     * [0.4] [0.1] [0.8] [-0.5] [-0.5]
     * </code>
     *
     * @return Uma <b>cópia</b> do cromossomo deste canal.
     */
    public double[] getCromossomo() {
        double[] c = new double[this.cromossomo.length];
        //copia os genes
        System.arraycopy(cromossomo, 0, c, 0, cromossomo.length);
        return c;
    }

    @Override
    public String toString() {
        String s = "Canal_" + id + ": ";
        NumberFormat numberFormat = new DecimalFormat("#.##");

        double[] k = getCromossomo();
        for (int i = 0; i < k.length; i++) {
            s += "[" + numberFormat.format(k[i]) + "]";
        }

        if (isVazio()) {
            s += " (Vazio)";
        } else {
            s += " | ";
            for (Sensor sensore : this.sensores) {
                if (sensore != null) {
                    s += sensore.getId() + " → ";
                }
            }
            s += "∅";
            s += " | " + this.calcularComprimento();
        }

        return s;
    }

    /**
     * @return Representação amigável deste canal, indicando as conexões entre
     * os sensores no formato: * → 1 → 2 ... → ∅
     */
    public String getDecodificacao() {
        String s = "";
        for (Sensor sensore : this.sensores) {
            if (sensore != null) {
                if(sensore.isInterrogador()){
                    s += "* → ";
                } else {
                    s += sensore.getId() + " → ";
                }
            }
        }
        s += "∅";
        return s;
    }

    /**
     * @return the id
     */
    public int getID() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setID(int id) {
        this.id = id;
    }

    /**
     * @return the usado
     */
    public boolean isUsado() {
        return usado;
    }

    /**
     * @param usado the usado to set
     */
    public void setUsado(boolean usado) {
        this.usado = usado;
    }

}
