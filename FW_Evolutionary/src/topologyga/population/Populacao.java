/*
 * Copyright (C) 2014 Manoel Afonso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package topologyga.population;

import java.util.ArrayList;

/**
 * População do algoritmo genético.
 *
 * @author Manoel Afonso Filho
 */
public class Populacao {

    /**
     * Quantidade de indivíduos presentes na população.
     */
    public static int numIndividuos = 10;

    /**
     * Conjunto de todos os indivíduos presentes nesta população.
     */
    private final ArrayList<Individuo> individuos;

    /**
     * Cria uma população com nenhum indivíduo presente.
     */
    public Populacao() {
        this.individuos = new ArrayList<>();
    }

    /**
     * @return O indivíduo com maior fitness.
     */
    public Individuo getFittest() {
        double maiorFitness = Double.NEGATIVE_INFINITY;
        Individuo ind = null;
        for (Individuo individuo : individuos) {
            double fTemp = individuo.getFitness();
            if (fTemp > maiorFitness) {
                maiorFitness = fTemp;
                ind = individuo;
            }
        }
        return ind;
    }

    /**
     * @param indice Índice do indivíduo na população.
     * @return Uma <b>referência</b> ao indivíduo no índice dado.
     */
    public Individuo getIndividuoAt(int indice) {
        return this.individuos.get(indice);
    }

    /**
     * Retorna o fitness do indivíduo presente no {@code indice} dado.
     *
     * @param indice Índice do individuo.
     * @return O fitness do indivíduo.
     */
    public double getFitnessIndividuoAt(int indice) {
        return this.individuos.get(indice).getFitness();
    }

    /**
     * Substitui o individuo na posição dada pelo novo indivíduo.
     *
     * @param indice Índice do indivíduo a ser substituido.
     * @param ind Novo indivíduo.
     */
    public void setIndividuoAt(int indice, Individuo ind) {
        this.individuos.set(indice, ind);
    }

    /**
     * Armazena um indivíduo na população.
     *
     * @param ind Indivíduo a ser adicionado.
     */
    public void addIndividuo(Individuo ind) {
        this.individuos.add(ind);
    }

    /**
     * Armazena um array de indivíduos na população.
     *
     * @param ind Indivíduos a serem adicionados.
     */
    public void addIndividuo(Individuo... ind) {
        for (Individuo individuo : ind) {
            this.addIndividuo(individuo);
        }
    }
    
    /**
     * Adiciona os indivíduos da população {@code pop} nesta. Note que
     * alterações feitas nos indivíduos adicionados <b>serão refletidos</b> em
     * {@code pop}.
     * @param pop População com os indivíduos a serem adicionados a esta.
     */
    public void addIndividuo(Populacao pop){
        ArrayList<Individuo> inds = pop.getIndividuos();
        for (Individuo individuo : inds) {
            this.individuos.add(individuo);
        }
    }
    
    /**
     * Adiciona os indivíduos das populações dadas nesta população.
     * @param pop Populações com os indivíduos a serem adicionados a esta.
     * @see Populacao#addIndividuo(topologyga.population.Populacao) 
     */
    public void addIndividuo(Populacao... pop){
        for (Populacao populacao : pop) {
            this.addIndividuo(populacao);
        }
    }

    /**
     * Verifica se o indivíduo já está na população.
     *
     * @param ind Indivíduo alvo.
     * @return true caso já esteja presente, false caso contrário.
     */
    public boolean contemIndividuo(Individuo ind) {
        return this.individuos.contains(ind);
    }

    /**
     * @return O valor médio dos fitnesses de todos os indivíduos desta
     * população.
     */
    public double mediaFitness() {
        double soma = 0;
        for (Individuo individuo : individuos) {
            soma += individuo.getFitness();
        }
        return soma / individuos.size();
    }

    /**
     * @return A quantidade de indivíduos presentes nesta população.
     */
    public int getTamanhoPop() {
        return this.individuos.size();
    }

    /**
     * @return Uma <b>referência</b> ao conjunto de indivíduos desta população.
     * Alterações feitas nos indivíduos retornados são refletidas nos indivíduos
     * desta população.
     */
    public ArrayList<Individuo> getIndividuos() {
        return individuos;
    }
    
    /**
     * Gera um array com todos os indivíduos presentes nesta população.
     * @return Array com os indivíduos.
     */
    public Individuo[] toArray(){
        Individuo[] inds = new Individuo[this.individuos.size()];
        for (int i = 0; i < inds.length; i++) {
            inds[i] = this.individuos.get(i);
        }
        
        return inds;
    }

    @Override
    public String toString() {
        String s = "Preenchidos/Capacidade: " + this.individuos.size() + "/" + Populacao.numIndividuos + "\n";
        s += "----------------------------\n";
        for (Individuo individuo : individuos) {
            s += individuo.toString() + "\n";
        }
        s += "----------------------------";
        return s;
    }

}
