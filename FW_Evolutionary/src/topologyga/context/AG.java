/*
 * Copyright (C) 2014 Manoel Afonso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package topologyga.context;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Properties;
import topologyga.population.Individuo;
import topologyga.population.Populacao;
import topologyga.strategy.Cruzamento;
import topologyga.strategy.Fitness;
import topologyga.strategy.Inicializacao;
import topologyga.strategy.Mutacao;
import topologyga.strategy.Replacement;
import topologyga.strategy.Selecao;
import topologyga.util.MersenneTwisterFast;

/**
 * Especificação da estrutura do Algoritmo Genético.
 *
 * @author Manoel Afonso
 */
public abstract class AG {

    /**
     * Quantidade de gerações do algoritmo.
     */
    private static int numGeracoes = 500;

    /**
     * Contador especificando a geração atual.
     */
    private static int geracaoAtual = 0;

    /**
     * Se verdadeiro, indica que o algoritmo usa elitismo.
     */
    private boolean elitismo = false;

    /**
     * Arquivo de configuração padrão.
     */
    public static final String AG_DEFAULT_CONFIG_FILE = "ag_config.properties";

    /**
     * Gerador de números aleatórios.
     */
    public static MersenneTwisterFast prng;

    /**
     * Melhor solução encontrada até a presente geração. Aplicável caso o AG
     * esteja no modo elitista.
     */
    private Individuo melhorSolucao;

    /**
     * População do AG.
     */
    private Populacao populacao;

    /**
     * Interface para o operador de fitness.
     */
    private Fitness fitness;

    /**
     * Interface para a geração da população inicial.
     */
    private Inicializacao inicializacao;

    /**
     * Interface para o operador de Seleção.
     */
    private Selecao selecao;

    /**
     * Interface para o operador de Cruzamento.
     */
    private Cruzamento cruzamento;

    /**
     * Interface para o operador de Mutação.
     */
    private Mutacao mutacao;

    /**
     * Interface para o operador de seleção dos sobreviventes.
     */
    private Replacement replacement;

    /**
     * Cria uma instância do algoritmo genético utilizando os operadores dados e
     * inicializa o gerador de números aleatórios.
     *
     * @param i Operador de inicialização.
     * @param s Operador de seleção.
     * @param c Operador de cruzamento.
     * @param m Operador de mutação.
     * @param r Operador de replacement (seleção dos sobreviventes).
     * @param f Operador de fitness.
     */
    public AG(Inicializacao i, Selecao s, Cruzamento c, Mutacao m, Replacement r, Fitness f) {
        this.inicializacao = i;
        this.selecao = s;
        this.cruzamento = c;
        this.mutacao = m;
        this.replacement = r;
        this.fitness = f;
        AG.prng = new MersenneTwisterFast();
    }

    /**
     * Cria a população inicial que será modificada com o decorrer do AG. Este
     * método deve ser chamado <b>antes</b> de {@link AG#executarGeracao()}.
     */
    public abstract void gerarPopulacaoInicial();

    /**
     * Executa uma geração do AG modificando a população.
     */
    public abstract void executarGeracao();

    /**
     * Reinicia o estado do AG. Reinicia as gerações, o gerador de números
     * pseudo-aleatórios, a solução elitista e apaga a população.
     */
    public void reset() {
        AG.geracaoAtual = 0;
        AG.prng = new MersenneTwisterFast();
        this.melhorSolucao = null;
        this.populacao = null;
    }

    /**
     * Calcula o fitness de todos os indivíduos desta população.
     */
    public void calcularFitness() {
        this.calcularFitness(this.populacao);
    }

    /**
     * Calcula o fitness de todos os indivíduos da população {@code pop}.
     *
     * @param pop População contendo os indivíduos sem fitness calculado.
     */
    public void calcularFitness(Populacao pop) {
        ArrayList<Individuo> individuos = pop.getIndividuos();
        for (Individuo individuo : individuos) {
            individuo.setFitness(this.getFitness().calcular(individuo));
        }
    }
    
    /**
     * Calcula o fitness da população do AG utilizando uma implementação
     * de um fitness. Útil para o modo multiobjetivo quando o fitness precisa
     * ser calculado de maneira diferente da padrão.
     * @param fit Implementação de um operador de fitness.
     */
    public void calcularFitness(Fitness fit){
        this.calcularFitness(fit, this.populacao);
    }
    
    /**
     * Calcula o fitness da população {@code pop} utilizando o operador
     * de fitness {@code fit}.
     * @param fit Implementação de um fitness.
     * @param pop População cujos indivíduos terão o fitness calculado.
     */
    public void calcularFitness(Fitness fit, Populacao pop){
        ArrayList<Individuo> individuos = pop.getIndividuos();
        for (Individuo individuo : individuos) {
            individuo.setFitness(fit.calcular(individuo));
        }
    }
    
    public void cruzamento(Populacao matingPool, Populacao offspring){
        ArrayList<Individuo> indi = matingPool.getIndividuos();
        for (int i = 0; i < indi.size(); i += 2) {
            //Pais
            Individuo ind1 = indi.get(i);
            Individuo ind2 = indi.get(i + 1);

            //Verifica se haverá cruzamento
            double p = AG.prng.nextDouble();

            if (p < this.getCruzamento().getTaxaCruzamento()) {
                //Realiza o cruzamento
                Individuo[] filhos = this.getCruzamento().executar(ind1, ind2);
                offspring.addIndividuo(filhos);
            } else {
                //Passa os indivíduos sem os modificar
                offspring.addIndividuo(ind1, ind2);
            }
        }
    }
    
    public void mutacao(Populacao matingPool, Populacao offspring){
        for (int i = 0; i < offspring.getTamanhoPop(); i++) {
            //Verifica se haverá mutação
            double p = AG.prng.nextDouble();
            if (p < this.getMutacao().getTaxaMutacao()) {
                //Realiza a mutação
                Individuo mutado = this.getMutacao().executar(offspring.getIndividuoAt(i));
                offspring.setIndividuoAt(i, mutado);
            }
        }
    }

    /**
     * Carrega as configurações do AG a partir do arquivo de configuração
     * padrão. Este método deve ser chamado após os operadores terem sido
     * passados para esta classe.
     */
    public void carregarConfig() {
        this.carregarConfig(AG_DEFAULT_CONFIG_FILE);
    }

    /**
     * Carrega as configurações do AG a partir do arquivo de configuração
     * fornecido. Este método deve ser chamado após os operadores terem sido
     * passados para esta classe.
     *
     * @param arquivo Caminho do arquivo de configuração.
     */
    public void carregarConfig(String arquivo) {
        Properties prop = new Properties();
        InputStream input = null;

        try {
            input = new FileInputStream(arquivo);

            // Carrega o arquivo de propriedades
            prop.load(input);

            // Configura os valores dos operadores (a ordem é importante).
            AG.numGeracoes = Integer.parseInt(prop.getProperty("num_geracoes"));
            Populacao.numIndividuos = Integer.parseInt(prop.getProperty("tam_pop"));
            this.elitismo = Boolean.parseBoolean(prop.getProperty("elitismo"));
            this.selecao.setProporcao(Double.parseDouble(prop.getProperty("prop_sel")));
            this.cruzamento.setTaxaCruzamento(Double.parseDouble(prop.getProperty("taxa_cruzamento")));
            this.mutacao.setTaxaMutacao(Double.parseDouble(prop.getProperty("taxa_mutacao")));

        } catch (IOException ex) {
            System.out.println("Erro ao ler arquivo de configuração:\n" + ex);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    System.out.println("Erro ao ler arquivo de configuração:\n" + e);
                }
            }
        }
    }

    /**
     * @return Total de gerações que o AG irá executar.
     */
    public static int getNumGeracoes() {
        return AG.numGeracoes;
    }

    /**
     * @param valor Quantidade de gerações que o AG irá executar.
     */
    public static void setNumGeracoes(int valor) {
        AG.numGeracoes = valor;
    }

    /**
     * @return Inteiro indicando a geração atual.
     * <code>geraçãoAtual &isin; [0, {@link AG#numGeracoes}]</code>.
     */
    public static int getGeracaoAtual() {
        return AG.geracaoAtual;
    }

    /**
     * Após executar uma geração, atualiza o contador.
     */
    public static void incrementarGeracao() {
        AG.geracaoAtual += 1;
    }

    /**
     * @return true se o AG for elitista.
     */
    public boolean isElitismo() {
        return elitismo;
    }

    /**
     * @param elitismo true para tornar o AG elitista, false caso contrário.
     */
    public void setElitismo(boolean elitismo) {
        this.elitismo = elitismo;
    }

    /**
     * @return A população do AG.
     */
    public Populacao getPopulacao() {
        return populacao;
    }

    /**
     * @param populacao the populacao to set
     */
    public void setPopulacao(Populacao populacao) {
        this.populacao = populacao;
    }

    /**
     * @return the fitness
     */
    public Fitness getFitness() {
        return fitness;
    }

    /**
     * @param fitness the fitness to set
     */
    public void setFitness(Fitness fitness) {
        this.fitness = fitness;
    }

    /**
     * @return A interface de inicialização.
     */
    public Inicializacao getInicializacao() {
        return inicializacao;
    }

    /**
     * @param inicializacao O operador de inicialização.
     */
    public void setInicializacao(Inicializacao inicializacao) {
        this.inicializacao = inicializacao;
    }

    /**
     * @return A interface de seleção.
     */
    public Selecao getSelecao() {
        return selecao;
    }

    /**
     * @param selecao O operador de seleção.
     */
    public void setSelecao(Selecao selecao) {
        this.selecao = selecao;
    }

    /**
     * @return A interface de cruzamento.
     */
    public Cruzamento getCruzamento() {
        return cruzamento;
    }

    /**
     * @param cruzamento O operador de cruzamento.
     */
    public void setCruzamento(Cruzamento cruzamento) {
        this.cruzamento = cruzamento;
    }

    /**
     * @return A interface de mutação.
     */
    public Mutacao getMutacao() {
        return mutacao;
    }

    /**
     * @param mutacao O operador de mutação.
     */
    public void setMutacao(Mutacao mutacao) {
        this.mutacao = mutacao;
    }

    /**
     * @return A interface de seleção dos sobreviventes.
     */
    public Replacement getReplacement() {
        return replacement;
    }

    /**
     * @param replacement O operador de seleção dos sobreviventes.
     */
    public void setReplacement(Replacement replacement) {
        this.replacement = replacement;
    }

    /**
     * @return A melhor solução encontrada até agora.
     */
    public Individuo getMelhorSolucao() {
        return melhorSolucao;
    }

    /**
     * Caso o AG esteja no modo elitista, este método deve ser chamado ao fim de
     * cada geração para guardar o melhor indivíduo encontrado. Note que o
     * melhor indivíduo desta geração só será armazenado caso ele possua um
     * maior fitness que o melhor indivíduo já armazenado.
     */
    public void setMelhorSolucao() {
        Individuo ind = this.getPopulacao().getFittest();
        if (this.melhorSolucao != null && ind.getFitness() > this.melhorSolucao.getFitness()) {
            //Guarda pois é melhor que o indivíduo já guardado.
            this.melhorSolucao = ind;
        } else if (this.melhorSolucao == null) {
            //Primeiro indivíduo.
            this.melhorSolucao = ind;
        }
    }

    @Override
    public String toString() {
        String s = "NÚMERO DE GERAÇÕES: " + AG.numGeracoes + "\n"
                + "TAMANHO DA POPULAÇÃO: " + Populacao.numIndividuos + "\n"
                + "ELITISMO: " + (this.elitismo ? "Sim" : "Não") + "\n"
                + "INICIALIZAÇÃO: " + this.inicializacao.toString() + "\n"
                + "SELEÇÃO: " + this.selecao.toString() + "\n"
                + "CRUZAMENTO: " + this.cruzamento.toString() + "\n"
                + "MUTAÇÃO: " + this.mutacao.toString() + "\n"
                + "REPLACEMENT: " + this.replacement.toString();
        return s;
    }

}
