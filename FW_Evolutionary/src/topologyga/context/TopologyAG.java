/*
 * Copyright (C) 2014 Manoel Afonso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package topologyga.context;

import topologyga.population.Populacao;
import topologyga.strategy.Cruzamento;
import topologyga.strategy.Fitness;
import topologyga.strategy.Inicializacao;
import topologyga.strategy.Mutacao;
import topologyga.strategy.Replacement;
import topologyga.strategy.Selecao;

/**
 * Implementação do Algoritmo Genético.
 *
 * @author Manoel Afonso Filho
 */
public class TopologyAG extends AG {

    public TopologyAG(Inicializacao i, Selecao s, Cruzamento c, Mutacao m, Replacement r, Fitness f) {
        super(i, s, c, m, r, f);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void gerarPopulacaoInicial() {
        this.setPopulacao(this.getInicializacao().criarPopulacao());
        this.calcularFitness();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void executarGeracao() {
        /*
         Seleção
         */
        Populacao matingPool = this.getSelecao().executar(this.getPopulacao());

        //Descendentes
        Populacao offspring = new Populacao();

        /*
        Cruzamento
        */
        this.cruzamento(matingPool, offspring);

        /*
         Mutação
         */
        this.mutacao(matingPool, offspring);

        /*
         Calcula o fitness da offspring.
         */
        this.calcularFitness(offspring);

        /*
         Seleção dos sobreviventes (Replacement).
         */
        this.getReplacement().executar(this.getPopulacao(), offspring);
        
        /*
         Se o AG for elitista, guarda o melhor indivíudo.
         */
        if (this.isElitismo()) {
            this.setMelhorSolucao();
        }

        /*
         Incrementa a geração.
         */
        incrementarGeracao();
    }

}
