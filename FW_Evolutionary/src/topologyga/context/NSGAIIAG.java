/*
 * Copyright (C) 2014 Manoel Afonso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package topologyga.context;

import java.util.ArrayList;
import static topologyga.context.AG.incrementarGeracao;
import topologyga.multiobjetivo.NSGAII;
import topologyga.population.Individuo;
import topologyga.population.Populacao;
import topologyga.strategy.Cruzamento;
import topologyga.strategy.Dominacao;
import topologyga.strategy.Fitness;
import topologyga.strategy.Inicializacao;
import topologyga.strategy.Mutacao;
import topologyga.strategy.Objetivo;
import topologyga.strategy.Replacement;
import topologyga.strategy.Selecao;

/**
 *
 * @author Manoel Afonso
 */
public class NSGAIIAG extends AG {

    /**
     * Operador responsável por calcular os objetivos de cada indivíduo.
     */
    private final Objetivo objetivos;

    /**
     * Operador resposável por determinar as relações de dominância entre os
     * indivíduo.
     */
    private final Dominacao dominacao;

    /**
     * Cria um AG multiobjetivo baseado no NSGAII.
     *
     * @param i Operador de inicialização.
     * @param s Operador de seleção.
     * @param c Operador de cruzamento.
     * @param m Operador de mutação.
     * @param r Operador de selção dos sobreviventes.
     * @param f Operador para o cálculo do fitness de acordo com o NSGAII.
     * @param objt Operador para o cálculo dos obejtivos de cada indivíduo.
     * @param dom Operador para a análise de dominância entre os indivíduos.
     */
    public NSGAIIAG(Inicializacao i, Selecao s, Cruzamento c, Mutacao m, Replacement r, Fitness f, Objetivo objt, Dominacao dom) {
        super(i, s, c, m, r, f);
        this.objetivos = objt;
        this.dominacao = dom;
    }

    @Override
    public void gerarPopulacaoInicial() {
        //Cria a população inicial.
        this.setPopulacao(this.getInicializacao().criarPopulacao());

        //Calcula os objetivos de cada indivíduo.
        this.calcularObjetivos();

        //Calcula a frente de pareto e distância de multidão.
        this.nsgaII();

        //Calcula o fitness de cada indivíduo. Este fitness é dado por:
        this.calcularFitness();
    }

    @Override
    public void executarGeracao() {
        /*
         Seleção
         */
        Populacao matingPool = this.getSelecao().executar(this.getPopulacao());

        //Descendentes
        Populacao offspring = new Populacao();

        /*
         Cruzamento
         */
        this.cruzamento(matingPool, offspring);

        /*
         Mutação
         */
        this.mutacao(matingPool, offspring);

        /*
         Calcula os objetivos da offspring, pois os indivíduos foram alterados.
         */
        this.calcularObjetivos(offspring);

        /*
         Aplica o NSGAII na união da população atual com a offspring.
         */
        Populacao uniao = new Populacao();
        uniao.addIndividuo(this.getPopulacao(), offspring);
        this.nsgaII(uniao);

        /*
         Seleção dos sobreviventes (Replacement). Ela é aplicada na união
         da população atual com a offspring. O resultado é salvo na população.
         */
        this.getReplacement().executar(this.getPopulacao(), uniao);

        /*
         Calcula o fitness da população.
         Recalcula o fitness pois uma solução pode mudar de frente de pareto
         ou distância de multidão.
         */
        this.calcularFitness(this.getPopulacao());

        /*
         Incrementa a geração.
         */
        incrementarGeracao();
    }

    /**
     * Aplica o NSGAII na população deste AG.
     */
    private void nsgaII() {
        this.nsgaII(this.getPopulacao());
    }

    /**
     * Utiliza o NSGAII para calcular a frente de pareto e a distância de
     * multidão para cada individuo.
     *
     * @param pop População cujos indivíduos terão analisados para o cálculo da
     * frente de pareto e distância de multidão.
     */
    private void nsgaII(Populacao pop) {
        NSGAII.fastNonDominatedSort(pop.toArray(), this.dominacao);
        NSGAII.calculateCrowdingDistances(pop.toArray());
    }

    /**
     * Calcula os objetivos de cada indivíduo da população do AG.
     */
    private void calcularObjetivos() {
        this.calcularObjetivos(this.getPopulacao());
    }

    /**
     * Calcula os objetivos de cada indivíduo da população {@code pop} dada.
     *
     * @param pop População cujos indivíduos terão os objetivos calculados.
     */
    private void calcularObjetivos(Populacao pop) {
        ArrayList<Individuo> inds = pop.getIndividuos();
        for (Individuo individuo : inds) {
            this.objetivos.calcularObjetivos(individuo);
        }
    }

}
