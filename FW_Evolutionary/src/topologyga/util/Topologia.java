/*
 * Copyright (C) 2014 Manoel Afonso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package topologyga.util;

import topologyga.geometria.Poligono;
import topologyga.population.Sensor;
import topologyga.strategy.Inicializacao;

/**
 * Classe contendo todos os dados da topologia. Todos os dados devem estar
 * inicializados após o processo de {@link Inicializacao inicialização}.
 *
 * @author Manoel Afonso
 */
public class Topologia {

    /**
     * Quantidade máxima de canais que pode ser usada nesta topologia.
     */
    public static int qtdeMaxCanais = 0;

    /**
     * Quantidade máxima de sensores por canal.
     */
    public static int qtdeMaxSensorCanal = 0;

    /**
     * Conjunto de sensores disponíveis. O interrogador é tratado como um sensor
     * à parte, porém sua ID será <code>*</code> e seu lambda terá valor -1 para
     * não causar conflito com nenhum sensor propriamente dito. O interrogador
     * <b>sempre está na primeira posição do array</b>. Logo, a quantidade de
     * sensores existentes nesta topologia será {@code sensores.length - 1}.
     * Observe que nem todos os sensores deste conjunto estão necessariamente
     * conectados em alguma fibra.
     */
    public static Sensor[] sensores = null;

    /**
     * Conjunto de regiões com penalidade. Embora seja possível passar uma fibra
     * por essas regiões, o caminho feito deverá sofrer alguma penalidade. Essas
     * regiões podem representar, por exemplo, áreas verdes, campos abertos e
     * locais inseguros.
     */
    public static Poligono[] regioesPenalidade = null;

    /**
     * Conjunto de regiões intransponíveis. Não é possível passar uma fibra por
     * essas regiões. Essas regiões podem representar, por exemplo, paredes,
     * rios e prédios.
     */
    public static Poligono[] regioesIntrans = null;

    /**
     * Sumário de todos os dados da topologia.
     *
     * @return String com todos os valores da topologia.
     */
    @Override
    public String toString() {
        String s = "DADOS DA TOPOLOGIA";
        s += "Quantidade máxima de canais: " + qtdeMaxCanais + "\n"
                + "Quantidade máxima de sensor por canal: " + qtdeMaxSensorCanal + "\n"
                + "Localização do interrogador: " + sensores[0].getLocal().toString() + "\n"
                + "Sensores:\n";
        for (int i = 1; i < sensores.length; i++) {
            Sensor sensor = sensores[i];
            s += "\t" + sensor.toString() + "\n";
        }
        s += "Regiões com penalidade:\n";
        for (Poligono poligono : regioesPenalidade) {
            s += "\t" + poligono.toString() + "\n";
        }
        s += "Regiões intransponíveis:\n";
        for (Poligono poligono : regioesIntrans) {
            s += "\t" + poligono.toString() + "\n";
        }

        return s;
    }

}
