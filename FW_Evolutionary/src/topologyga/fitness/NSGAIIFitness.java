/*
 * Copyright (C) 2014 Manoel Afonso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package topologyga.fitness;

import topologyga.population.Individuo;
import topologyga.strategy.Fitness;

/**
 * Implementação do fitness do NSGAII. O algoritmo NSGAII calcula o fitness
 * da seguinte forma:<br/>
 * {@code F = ( 1 / frente_pareto ) + distancia_multidao}
 * @author Manoel Afonso
 */
public class NSGAIIFitness implements Fitness {

    @Override
    public double calcular(Individuo ind) {
        double f = (1 / ind.getParetoFront()) + ind.getCrowdingDistance();
        
        return f;
    }
    
}
