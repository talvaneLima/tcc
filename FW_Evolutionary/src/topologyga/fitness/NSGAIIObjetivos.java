/*
 * Copyright (C) 2014 Manoel Afonso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package topologyga.fitness;

import topologyga.population.Individuo;
import topologyga.strategy.Objetivo;

/**
 * Calcula os objetivos de cada indivíduo.
 * @author Manoel Afonso
 */
public class NSGAIIObjetivos extends SensorFitness implements Objetivo {

    public NSGAIIObjetivos() {
        super();
    }

    @Override
    public void calcularObjetivos(Individuo ind) {
        double comprimento = super.calcular(ind);
        double lambdasDist = ind.qtdeLambdasDistintos();

        double[] objetivos = new double[2];
        objetivos[0] = comprimento;
        objetivos[1] = lambdasDist;

        ind.setObjetivos(objetivos);
    }

}
