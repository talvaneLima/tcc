/*
 * Copyright (C) 2014 Manoel Afonso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package topologyga.fitness;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import topologyga.geometria.Poligono;
import topologyga.geometria.Util;
import topologyga.population.Canal;
import topologyga.population.Individuo;
import topologyga.population.Sensor;
import topologyga.strategy.Fitness;
import topologyga.util.Topologia;

/**
 * Classe para o cálculo do fitness de um indivíduo. São levadas em consideração
 * as passagens em regiões verdes, a abrangência, a utilização das fibras e os
 * erros de multiplexação.
 *
 * @author Manoel Afonso
 */
public class SensorFitness implements Fitness {

    /**
     * Quantidade de sensores disponíveis na topologia, desconsiderando o
     * interrogador.
     */
    private final int totSensor;

    /**
     * Cria um operador de fitness configurando a quantidade total de sensores
     * disponíveis na topologia, desconsiderando o interrogador.
     */
    public SensorFitness() {
        //Retiramos o interrogador.
        this.totSensor = Topologia.sensores.length - 1;
    }

    /**
     * Calcula o fitness do indivíduo. O valor do fitness é baseado no
     * comprimento total de todas as fibras de {@code ind} como se segue:<br>
     * {@code F(I) = L(I) - L(I) * V(I) - L(I) * A(I) - L(I) * U(I) - L(I) * M(I)}
     * <br>
     * Assim, o melhor caso ocorre quando todas as penalidades são 0:<br>
     * {@code F(I) = L(I)} <br>
     * O pior caso, quando todas as penalidades são 1:<br>
     * {@code F(I) = L(I) - 4 * L(I) = -3 * L(I)}<br>
     * Portanto, o AG deverá maximizar o fitness.
     *
     * @param ind Indivíduo a ser analisado.
     * @return O fitness {@code F(I)} no intervalo {@code [-3*L(I), L(I)]}.
     */
    @Override
    public double calcular(Individuo ind) {
        double pp = this.regioesProibidas(ind);
        double pv = this.regioesPenalidades(ind);
        double pa = this.abrangencia(ind);
        double pu = this.utilizacao(ind);
        double pm = this.multiplexacao(ind);
        double l = ind.getComprimentoTotal();

        //Calcula o fitness transformando as penalidades em distâncias.
        double f = l - l * pp - l * pv - l * pa * 3 - l * pu * 2 - l * pm;

        return f;
        //FIXME alterar a assinatura: retorna void e seta o fitness aqui mesmo.
    }
    
    /**
     * Penalidade das passagens em regiões proibidas. Se algum trecho de algum
     * canal do indivíduo passar por uma região proibida, então a penalidade é
     * 1, caso contrário é 0. Se utiliza os valores extremos pois passar em uma
     * região proibida gera uma solução inválida.
     *
     * @param ind Indivíduo a ser analisado.
     * @return 1 se algum trecho de alguma fibra passa por uma região proibida,
     * 0 caso contrário.
     */
    private int regioesProibidas(Individuo ind) {
        Canal[] canais = ind.getCanais();
        //Percorre os canais.
        for (Canal canal : canais) {
            Sensor[] sensores = canal.getSensores();
            //Percorre os sensores.
            for (int i = 0; i < sensores.length; i++) {
                /*
                 Como a distância é feita par a par, vamos até a penúltima posição e
                 verificamos se há um próximo sensor para calcular a distância.
                 */
                if (i < sensores.length - 1 && sensores[i + 1] != null) {
                    //Obtém um par de sensores e calcula a distância do trecho.
                    Sensor s1 = sensores[i];
                    Sensor s2 = sensores[i + 1];
                    //Verifica se o trecho s1-s2 cruza em alguma região proibida.
                    for (Poligono regioesIntran : Topologia.regioesIntrans) {
                        if (Util.retaCruzaPoligono(s1.getLocal(), s2.getLocal(), regioesIntran)) {
                            return 1;
                        }
                    }
                }
            }
        }
        return 0;
    }

    /**
     * Porcentagem de trechos que passam por uma região com penalidade. Um
     * trecho é a distância entre dois sensores. Assim, em um indivíduo cuja
     * configuração gera C canais totalizando T trechos, então é calculado
     * quantos {@code t} trechos passam por um região com penalidade.
     *
     * @param ind Indivíduo a ser analisado.
     * @return Um valor no intervalo [0,1] equivalente à média das penalidades
     * de cada canal do indivíduo {@code ind}.
     */
    private double regioesPenalidades(Individuo ind) {
        //Por padrão, a penalidade é 0 (não cruzou nenhuma região).
        double penalidade;
        //Quantidade total de trechos presentes em todas as fibras.
        int totalTrechos = 0;
        //Quantidade de trechos que passam por uma região com penalidade.
        int trechosPen = 0;

        //Percorre os canais.
        for (Canal canal : ind.getCanais()) {
            Sensor[] sensores = canal.getSensores();
            //Percorre os sensores.
            for (int i = 0; i < sensores.length; i++) {
                /*
                 Como a distância é feita par a par, vamos até a penúltima posição e
                 verificamos se há um próximo sensor para calcular a distância.
                 */
                if (i < sensores.length - 1 && sensores[i + 1] != null) {
                    //Obtém um par de sensores e calcula a distância do trecho.
                    Sensor s1 = sensores[i];
                    Sensor s2 = sensores[i + 1];
                    //Incrementa a quantidade de trechos
                    totalTrechos++;
                    //Verifica se o trecho s1-s2 cruza em alguma região com penalidade.
                    for (Poligono regioesPen : Topologia.regioesPenalidade) {
                        if (Util.retaCruzaPoligono(s1.getLocal(), s2.getLocal(), regioesPen)) {
                            //Incrementa a quantidade de trechos com penalidade.
                            trechosPen++;
                        }
                    }
                }
            }
        }

        //Ao fim, calculamos a porcentagem de trechos que passam por regiões
        //com penalidade.
        penalidade = ((double) trechosPen / totalTrechos);

        return penalidade;
    }

    /**
     * Calcula a penalidade da abrangência de cada indivíduo. A abrangência
     * consiste na porcentagem de sensores que todos os canais do indivíduo
     * conectam. Por exemplo, se em uma topolgia com 10 sensores, um indivíduo
     * conecta 7, então a abragência é de 0.7 (70%) e, portanto, sua penalidade
     * será 0.3 (30%).
     *
     * @param ind Indivíduo a ser analisado.
     * @return Penalidade da abrangência das conexões no intervalo [0,1].
     */
    private double abrangencia(Individuo ind) {
        Canal[] canais = ind.getCanais();
        //Quantidade total de sensores conectados.
        int qtdeConectada = 0;
        //Verifica os sensores de todos os canais.
        for (Canal canal : canais) {
            Sensor[] sensores = canal.getSensores();
            //Procura por sensores que estão instalados neste canal.
            for (Sensor sensore : sensores) {
                if (sensore != null && !sensore.isInterrogador()) {
                    //Incrementa a quantidade total de sensores instalados.
                    qtdeConectada++;
                }
            }
        }

        //Calculamos a porcentagem de sensores não-conectados.
        double abran = 1 - ((double) qtdeConectada / this.totSensor);

        return abran;
    }

    /**
     * Penalidade referente a taxa de não utilização das fibras do indivíduo. A
     * taxa de não-utilização da fibra consiste na porcentagem total de sensores
     * não instalados no total de espaços alocáveis de todas as fibras.
     *
     * @param ind Indivíduo a ser analisado.
     * @return Um valor no intervalo [0,1] referente à penalidade de utilização.
     */
    private double utilizacao(Individuo ind) {
        //Quantidade de espaços alocáveis em todos os canais.
        int qtdeEspacos = 0;
        //Quantidade total de sensores instalados.
        int qtdeInstalada = 0;

        Canal[] canais = ind.getCanais();
        for (Canal canal : canais) {
            //Acumula a quantidade de espaços alocáveis
            qtdeEspacos += canal.getSensores().length;
            //Conta quantos sensores estão instalados.
            Sensor[] sensores = canal.getSensores();
            for (Sensor sensor : sensores) {
                if (sensor != null && !sensor.isInterrogador()) {
                    qtdeInstalada++;
                }
            }
        }

        //Calcula a porcentagem de espaços não utilizados.
        double penalidade = 1 - ((double) qtdeInstalada / qtdeEspacos);

        return penalidade;
    }

    /**
     * Calcula a penalidade de multiplexação das fibras de um indivíduo. Cada
     * fibra recebe uma penalidade referente a erros de multiplexação e a média
     * dessas penalidades é retornada. A penalidade de cada fibra é calculada da
     * seguinte forma: calcula-se o número de vezes que um comprimento de onda
     * aparece na fibra. Se ele aparecer mais de uma vez, então há um erro de
     * multiplexação. O valor do erro de multiplexação é a quantidade de vezes
     * que um dado lambda aparece na fibra. Calcula-se a taxa de erros de
     * multiplexação em um canal. Por fim, calcula-se a média das taxas de
     * erros.
     *
     * @param ind Indivíduo a ser analisado.
     * @return Penalidade de multiplexação no intervalo [0,1].
     */
    //TODO a penalidade poderia ser a maior penalidade dentre as fibras e não
    //a média delas?
    private double multiplexacao(Individuo ind) {
        Canal[] canais = ind.getCanais();
        //Somatório das penalidades de cada fibra.
        double somaErrosTotal = 0;

        for (Canal canal : canais) {
            //Somatório dos erros de multiplexação (quantidade de vezes que cada
            //lambda aparece neste canal).
            double somErros = 0;
            Sensor[] sensores = canal.getSensores();
            //Lista com todos os lambdas presentes na fibra, podendo haver
            //repetições.
            List<Double> freqLambdas = new ArrayList<>();
            for (Sensor sensor : sensores) {
                if (sensor != null) {
                    freqLambdas.add(sensor.getLambda());
                }
            }

            //Lista com todos os lambdas distintos da fibra.
            List<Double> distLambdas = new ArrayList<>();

            //Remove as repetições para por na nova lista.
            HashSet<Double> hs = new HashSet<>();
            hs.addAll(freqLambdas);
            distLambdas.addAll(hs);

            //Para cada lambda distinto, calculamos a sua frequência na fibra.
            for (Double lambda : distLambdas) {
                int f = Collections.frequency(freqLambdas, lambda);
                //Se houver repetição, acumula o erro de multiplexação.
                if (f > 1) {
                    somErros += f;
                }
            }

            //Calculamos a porcentagem de erros deste canal
            double taxaErrosCanal = ((double) somErros / sensores.length);

            //Acumulamos a penalidade deste canal.
            somaErrosTotal += taxaErrosCanal;
        }

        //A penalidade é a média das penalidades dos canais.
        double penalidade = somaErrosTotal / canais.length;

        return penalidade;
    }

    /**
     * Calcula o logaritmo de um número em uma {@code base} específica.
     *
     * @param base Base do logaritmo.
     * @param num Número do qual se quer o logaritmo.
     * @return Logaritmo de {@code num} na base {@code base}.
     */
    public static double logOfBase(double base, double num) {
        return Math.log(num) / Math.log(base);
    }

}
