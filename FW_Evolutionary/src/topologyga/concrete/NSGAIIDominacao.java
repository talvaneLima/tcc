/*
 * Copyright (C) 2014 Manoel Afonso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package topologyga.concrete;

import topologyga.population.Individuo;
import topologyga.strategy.Dominacao;

/**
 * O indivíduo {@code A} domina o indivíduo {@code B} se {@code A} tiver
 * menor comprimento total e maior número de lambdas distintos.
 * O comprimento total deve ser o objetivo de índice {@code 0} e número de
 * lambdas distintos deve ser de índice {@code 1}.
 * @author Manoel Afonso
 */
public class NSGAIIDominacao implements Dominacao {

    @Override
    public boolean dominates(Individuo ind1, Individuo ind2) {
        double[] objetivos1 = ind1.getObjetivos();
        double[] objetivos2 = ind2.getObjetivos();
        
        return objetivos1[0] < objetivos2[0] && objetivos1[1] > objetivos2[1];
    }
    
}
