/*
 * Copyright (C) 2014 Manoel Afonso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package topologyga.concrete;

import topologyga.context.AG;
import topologyga.population.Individuo;
import topologyga.strategy.Mutacao;

/**
 * Implementação da mutação gaussiana.
 *
 * @author Manoel Afonso
 */
public class SensorMutacao extends Mutacao {

    @Override
    public Individuo executar(Individuo ind) {
        double[] cromossomo = ind.getCromossomo();

        //Altera todos os genes do cromossomo, exceto o interrogador e genes
        //inativos
        for (int i = 0; i < cromossomo.length; i++) {
            if (cromossomo[i] != Individuo.GENE_INTERROGADOR
                    && cromossomo[i] != Individuo.GENE_INATIVO) {
                cromossomo[i] = mutacaoGaussiana(cromossomo[i]);
            }
        }

        return ind;
    }

    /**
     * Calcula um valor gaussiano aleatório com desvio padrão igual a 0.2 e
     * média igual a {@code media}. Valores acima de 1 são arredondados para 1 e
     * abaixo de 0, arredondados para 0.
     *
     * @param media Média da distribuição gaussiana.
     * @return Um valor gaussiano aleatório com desvio padrão igual a 0.2.
     */
    private double mutacaoGaussiana(double media) {
        double m = AG.prng.nextGaussian() * 0.2 + media;
        if (m < 0) {
            m = 0;
        } else if (m > 1) {
            m = 1;
        }
        return m;
    }

    @Override
    public String toString() {
        return "Probabilidade de mutação: " + this.getTaxaMutacao();
    }

}
