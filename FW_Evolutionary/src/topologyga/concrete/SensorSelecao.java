/*
 * Copyright (C) 2014 Manoel Afonso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package topologyga.concrete;

import topologyga.context.AG;
import topologyga.population.Populacao;
import topologyga.strategy.Selecao;

/**
 * Implementação da seleção por torneio binária. Nesta implementação, são
 * selecionados dois indivíduos aleatoriamente para competir. Com um tamanho de
 * ringue pequeno, evita-se que sempre os mesmos melhores indivíduos sejam
 * selecionados, diminuindo a diversidade.
 *
 * @author Manoel Afonso
 */
public class SensorSelecao extends Selecao {

    /**
     * Seleciona x membros da população atual para o Crossover e Mutação. São
     * escolhidos dois indivíduos, aleatoriamente, da população. Aquele que
     * possuir maior fitness entra no mating pool.
     *
     * @param pop População atual.
     * @return Indivíduos selecionados.
     */
    @Override
    public Populacao executar(Populacao pop) {
        //Alterações feitas aos indivíduos do mating pool são refletidas na
        //população.
        Populacao matingPool = new Populacao();

        //Seleciona a quantidade especificada de indivíduos.
        for (int i = 0; i < this.getTotalIndividuos(); i++) {
            //Seleciona dois indivíduos aleatoriamente (com reposição).
            int id1 = (int) (AG.prng.nextDouble() * (pop.getTamanhoPop() - 1));
            int id2 = (int) (AG.prng.nextDouble() * (pop.getTamanhoPop() - 1));

            //Verifica quem tem maior fitness e o adiciona ao mating pool.
            double fitness1 = pop.getFitnessIndividuoAt(id1);
            double fitness2 = pop.getFitnessIndividuoAt(id2);

            if (fitness1 >= fitness2) {
                matingPool.addIndividuo(pop.getIndividuoAt(id1));
            } else {
                matingPool.addIndividuo(pop.getIndividuoAt(id2));
            }
        }

        return matingPool;
    }

}
