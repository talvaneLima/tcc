/*
 * Copyright (C) 2014 Manoel Afonso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package topologyga.concrete;

import java.util.Arrays;
import java.util.Collections;
import topologyga.compator.DistanciaMultidaoComparator;
import topologyga.compator.ParetoFrontComparator;
import topologyga.multiobjetivo.NSGAII;
import topologyga.population.Individuo;
import topologyga.population.Populacao;
import topologyga.strategy.Replacement;

/**
 * Seleção dos sobreviventes do NSGAII.
 *
 * @author Manoel Afonso
 */
public class NSGAIIReplacement implements Replacement {

    /**
     * Executa a seleção dos sobreviventes do NSGAII. {@code offspring} é a
     * <b>união entre a população original e a offspring</b> gerada pelos
     * operadores de variação (crossover, mutação, etc). O resultado da seleção
     * dos sobreviventes é salvo na população {@code original}.
     *
     * @param original População original, cujos indivíduos serão substituídos
     * de acordo com o resultado da seleção dos sobreviventes.
     * @param offspring União da população original com a sua offspring.
     */
    @Override
    public void executar(Populacao original, Populacao offspring) {
        //Transforma em array.
        Individuo[] inds = offspring.toArray();

        //Ordena em ordem ascendente de acordo com a frente de pareto.
        Collections.sort(Arrays.asList(inds), new ParetoFrontComparator());

        //Indivíduos que serão retornados.
        Individuo[] sobreviventes = new Individuo[Populacao.numIndividuos];

        int i = 1;
        int firstPos = 0;
        int size;
        int missingChromosomes = Populacao.numIndividuos;

        while (missingChromosomes != 0) {
            size = NSGAII.getParetoFrontSize(inds, firstPos, i);
            Individuo F[] = new Individuo[size];
            System.arraycopy(inds, firstPos, F, 0, F.length);

            NSGAII.calculateCrowdingDistances(F);

            if (size <= missingChromosomes) {
                System.arraycopy(F, 0, sobreviventes, firstPos, F.length);
                missingChromosomes -= size;
            } else {
                //LOOK ordem descendente
                Collections.sort(Arrays.asList(F), new DistanciaMultidaoComparator());
                System.arraycopy(F, 0, sobreviventes, firstPos, missingChromosomes);
                missingChromosomes = 0;
            }
            firstPos += size;
            i++;
        }

        //Copia de volta para a população original.
        for (int j = 0; j < original.getTamanhoPop(); j++) {
            original.setIndividuoAt(j, sobreviventes[j]);
        }
    }

}
