/*
 * Copyright (C) 2014 Manoel Afonso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package topologyga.concrete;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import topologyga.context.AG;
import topologyga.geometria.Poligono;
import topologyga.geometria.Ponto;
import topologyga.geometria.Util;
import topologyga.population.Canal;
import topologyga.population.Individuo;
import topologyga.population.Populacao;
import topologyga.population.Sensor;
import topologyga.strategy.Inicializacao;
import topologyga.util.Topologia;

/**
 * Realiza a criação dos indivíduos da população. Para criar indivíduos válidos,
 * é utilizado o raio de alcance, para que apenas a vizinhança mais próxima do
 * sensor seja considerada. Para formar o caminho de um indivíduo, é necessário
 * um mapa da região desejada. Esse mapa deve conter:<br>
 * <ul>
 * <li>Quantidade de canais disponíveis</li>
 * <li>Quantidade máxima de sensores por canal</li>
 * <li>Localização dos interrogadores ópticos</li>
 * <li>Localização e comprimento de onda dos sensores</li>
 * <li>Regiões intransponíveis (impossível passar)</li>
 * <li>Regiões indesejáveis (recebe uma penalidade caso a fibra passe)</li>
 * </ul>
 * Para facilitar a entrada dos dados acima, deve-se fornecer um arquivo de
 * configuração que contenha o mapa da topologia.
 *
 * @see SensorInicializacao#andar(topologyga.population.Canal,
 * topologyga.population.Sensor) Caminhamento na fibra
 *
 * @author Manoel Afonso
 */
public class SensorInicializacao implements Inicializacao {

    /**
     * Raio de alcance. Serve para delimitar os vizinhos mais próximos de um
     * sensor.
     */
    private final double raioAlcance = 0;

    /**
     * Conjunto de canais para cada indivíduo a ser criado.
     */
    private ArrayList<Canal> canais;

    /**
     * Cria o operador de configuração e lê todos os atributos presentes no mapa
     * da região.
     *
     * @param prop Arquivo de configuração do operador contendo o mapa.
     * @see SensorInicializacao#carregarConfig(java.lang.String) carregarConfig
     */
    public SensorInicializacao(String prop) {
        this.carregarConfig(prop);
    }

    @Override
    public Populacao criarPopulacao() {
        //Calcula o raio de alcance
        //this.raioAlcance = this.calcularRaioAlcance();

        //Calcula os vizinhos de cada sensor
        this.calcularAdjacencias();

        //Nova população
        Populacao pop = new Populacao();

        //Preenche toda a população com indivíduos aleatórios.
        for (int i = 0; i < Populacao.numIndividuos; i++) {
            pop.addIndividuo(this.criarIndividuo());
        }

        return pop;
    }

    /**
     * Cria um indivíduo aleatório. O processo de criação consiste em passar as
     * fibras em sensores aleatórios, respeitando as invariantes, até não ser
     * mais possível. As fibras, que por definição sempre saem do interrogador,
     * terão que passar obrigatória e primeiramente por um adjacente do
     * interrogador. Somente a partir daí, os próximos sensores serão escolhidos
     * de forma aleatória. Vale ressaltar que o conjunto de fibras de cada
     * indivíduo não vai passar necessariamente por todos os sensores, nem vai
     * garantir que cada fibra tenha sensores com lambdas distintos.
     *
     * @return Um indivíduo aleatório.
     * @see SensorInicializacao#andar(topologyga.population.Canal,
     * topologyga.population.Sensor) Caminhamento da fibra
     */
    private Individuo criarIndividuo() {
        //Aloca um novo conjunto dos canais disponíveis para este indivíduo.
        this.canais = new ArrayList<>();
        for (int i = 0; i < Topologia.qtdeMaxCanais; i++) {
            Canal c = new Canal(Topologia.qtdeMaxSensorCanal);
            c.setID(i);
            this.canais.add(c);
        }

        //Obtém o primeiro canal disponível.
        Canal c = canais.get(0);
        //Interrogador de onde sairão todas as fibras.
        Sensor interrogador = Topologia.sensores[0];
        //Como ele sempre é fonte, o marcamos como instalado.
        interrogador.setInstalado(true);
        //Obtém o índice do próximo adjacente não visitado do interrogador.
        int indVizinho = getIndexProxVizinhoNaoVisitado(interrogador);

        //Caminha enquanto houver vizinhos não visitados e canais disponíveis.
        while (indVizinho != -1 && c != null) {
            /*
             O valor do gene do interrogador para que ele aponte para o índice
             do seu adjacente é calculado por:
             gene = indice_do_adjacente * (1.0 / total_de_adjacentes)
             Isso faz com que o gene sempre tenha um valor no limite dos
             intervalos de cada adjacente. Por exemplo, se o interrogador possui
             3 adjacentes, gene pode assumir os valores 0, 0.33 e 0.66 dependendo
             do índice do adjacente.
             */
            double gene = indVizinho * (1.0 / interrogador.getAdjacentes().size());
            //Instalamos o interrogador na fibra com o gene calculado.
            c.addSensor(interrogador, gene);

            //Obtemos o sensor que está no índice dado da lista.
            Sensor vizinho = interrogador.getAdjacentes().get(indVizinho);
            //Caminhamos a fibra aleatoriamente a partir do adjacente.
            this.andar(c, vizinho);

            //Após o caminhamento, não queremos mais usar esta fibra.
            c.setUsado(true);

            //Procura o próximo adjacente não visitado do interrogador.
            /*
             FIXME se todos os adjacentes de indVizinho já tiverem sido visitados
             então o caminhamento não avançará, e só ficaremos trocando os canais
             até acabar. Isso pode ser otimizado.
             */
            indVizinho = getIndexProxVizinhoNaoVisitado(interrogador);
            //Próximo canal.
            c = getProxCanalDisponivel(canais);
        }

        //Desinstala todos os sensores
        this.desinstalarSensores();

        //Cria o indivíduo
        Individuo ind = new Individuo(Topologia.qtdeMaxCanais, Topologia.qtdeMaxSensorCanal);
        //Insere os canais
        for (Canal canal : canais) {
            ind.adicionarCanal(canal);
        }

        return ind;
    }

    /**
     * Desinstala todos os sensores disponíveis para que se possa fazer o
     * caminhamento outra vez.
     */
    private void desinstalarSensores() {
        for (Sensor sensor : Topologia.sensores) {
            sensor.setInstalado(false);
        }
    }

    /**
     * Procura o primeiro canal que não está marcado como usado dentro da lista
     * fornecida.
     *
     * @param lista Lista de canais.
     * @return O primeiro canal encontrado que não está marcado como usado, ou
     * null caso todos os canais estejam usados.
     */
    private Canal getProxCanalDisponivel(List<Canal> lista) {
        for (Canal canal : lista) {
            if (!canal.isUsado()) {
                return canal;
            }
        }
        return null;
    }

    /**
     * Procura por um adjacente a {@code s} de forma aleatória, que ainda não
     * foi instalado em alguma fibra.
     *
     * @param s Sensor a ser analisado.
     * @return O índice de um adjacente a <code>s</code> ainda não visitado
     * obtido aleatoriamente, ou -1 caso todos os adjacentes já tenham sido
     * visitados.
     */
    private int getIndexProxVizinhoNaoVisitado(Sensor s) {
        //Conjunto dos índices que não foram visitados.
        ArrayList<Integer> indicesNaoVisitados = new ArrayList<>();

        //Procura que não foi visitado.
        ArrayList<Sensor> adj = s.getAdjacentes();
        for (int i = 0; i < adj.size(); i++) {
            Sensor tmp = adj.get(i);
            if (!tmp.isInstalado()) {
                indicesNaoVisitados.add(i);
            }
        }

        //Todos já estão visitados.
        if (indicesNaoVisitados.isEmpty()) {
            return -1;
        } else {
            //Sorteia um índice.
            int ind = AG.prng.nextInt(indicesNaoVisitados.size());
            return indicesNaoVisitados.get(ind);
        }

    }

    /**
     * Percorre os sensores aleatoriamente utilizando o canal dado. Retorna
     * quando encontrar um dead-end; ou acabou o limite da fibra; ou todos os
     * sensores já foram visitados. Após a visita do sensor, ele será marcado
     * como visitado e será instalado na fibra utilizando o gene aleatório
     * obtido.
     *
     * @param c Canal usado para percorrer a topologia.
     * @param s Sensor de partida.
     */
    private void andar(Canal c, Sensor s) {
        //Lista de adjacências do sensor que está sendo visitado
        ArrayList<Sensor> adjacentes = s.getAdjacentes();

        //Só anda se houver adjacentes, espaço no canal e adjacentes não visitados.
        if (!adjacentes.isEmpty() && c.haEspaco() && !adjacentesVisitados(s)) {
            //Valor do gene indicando o próximo sensor
            double r = AG.prng.nextDouble();

            //Calcula o índice do próximo do sensor.
            int proximo = s.geneParaIndice(r);
            Sensor proxSensor = adjacentes.get(proximo);

            //Marcamos o sensor como instalado e o adicionamos à fibra.
            s.setInstalado(true);
            c.addSensor(s, r);

            //Para continuar o caminhamento, o próximo sensor tem que estar livre.
            if (!proxSensor.isInstalado()) {
                this.andar(c, proxSensor);
            }
        }
    }

    /**
     * Verifica se todos os adjacentes do sensor já foram visitados (instalados
     * em alguma fibra).
     *
     * @param sensor Sensor a ser analisado.
     * @return true se todos os adjacentes já tiverem sido visitados, false caso
     * contrário.
     */
    private boolean adjacentesVisitados(Sensor sensor) {
        ArrayList<Sensor> adjacentes = sensor.getAdjacentes();
        int cont = 0;
        for (Sensor s : adjacentes) {
            if (s.isInstalado()) {
                cont++;
            }
        }
        return cont == adjacentes.size();
    }

    /**
     * Cria a lista de adjacências de cada sensor. A lista contém todos os
     * outros sensores da topologia. Dessa forma, um sensor pode se conectar com
     * qualquer outro.
     */
    private void calcularAdjacencias() {
        for (int i = 0; i < Topologia.sensores.length; i++) {
            Sensor s1 = Topologia.sensores[i];
            for (int j = 0; j < Topologia.sensores.length; j++) {
                if (i == j) {
                    continue; //desnecessário para o mesmo sensor
                }
                if(Topologia.sensores[j].getLambda() != s1.getLambda()){
                    s1.addAdjacente(Topologia.sensores[j]);
                }
            }
        }
    }

    /**
     * Verifica se a reta entre os pontos p1 e p2 passam por uma região
     * intransponível. Isso permite saber se p2 é alcançável a partir de p1 e
     * vice-versa.
     *
     * @param p1 Ponto p1.
     * @param p2 Ponto p2.
     * @return true se a reta cruza uma região intransponível, false caso
     * contrário.
     */
    private boolean cruzaRegiaoIntrans(Ponto p1, Ponto p2) {
        //Testa todos as regiões (polígonos).
        for (Poligono poligono : Topologia.regioesIntrans) {
            if (Util.retaCruzaPoligono(p1, p2, poligono)) {
                return true;
            }
        }
        //Não houve cruzamento.
        return false;
    }

    /**
     * Calcula o raio de alcance de um conjunto de pontos no plano. O raio de
     * alcance consiste na menor distância que delimita os vizinhos de um ponto.
     * É semelhante ao cálculo do raio de um grafo. Para cada ponto, se calcula
     * a distância euclidiana para todos os outros pontos e se armazena a maior.
     * Do conjunto dessas maiores distâncias, se retira a menor, sendo esta, o
     * raio de alcance.
     *
     * @return O raio de alcance.
     */
    private double calcularRaioAlcance() {
        //Conjunto com as maiores distâncias de cada sensor.
        List<Double> distancias = new ArrayList<>();

        //Array com todos os pontos de todos os sensores mais o interrogador.
        Ponto[] pontos = new Ponto[Topologia.sensores.length];

        //Posições do interrogador + sensores
        for (int i = 0; i < Topologia.sensores.length; i++) {
            pontos[i] = Topologia.sensores[i].getLocal();
        }

        //Calcula a maior distância euclidiana de cada ponto.
        for (Ponto ponto1 : pontos) {
            double maior = 0;
            for (Ponto ponto2 : pontos) {
                double dist = Util.distanciaEuclidiana(ponto1, ponto2);
                if (dist > maior) {
                    maior = dist;
                }
            }
            distancias.add(maior);
        }

        //Ordena em ordem crescente.
        Collections.sort(distancias);

        //Retorna a menor distância (primeira).
        return distancias.get(0);
    }

    /**
     * Configura os seguintes atributos a partir de um arquivo de
     * configuração:<br>
     * <ul>
     * <li>Quantidade de canais disponíveis</li>
     * <li>Quantidade máxima de sensores por canal</li>
     * <li>Localização dos interrogadores ópticos</li>
     * <li>Localização e comprimento de onda dos sensores</li>
     * <li>Regiões intransponíveis (impossível passar)</li>
     * <li>Regiões indesejáveis (recebe uma penalidade caso a fibra passe)</li>
     * </ul>
     *
     * @param arquivo Arquivo .properties.
     */
    private void carregarConfig(String arquivo) {
        Properties prop = new Properties();
        InputStream input = null;

        try {
            input = new FileInputStream(arquivo);

            // Carrega o arquivo de propriedades
            prop.load(input);

            // Configura os atributos da inicialização
            //Quantidade máxima de canais
            int qtdeMaxCanais = Integer.parseInt(prop.getProperty("max_channel"));

            //Interrogador óptico
            String[] cInt = prop.getProperty("loc_interr").split(",");
            int x = Integer.parseInt(cInt[0]);
            int y = Integer.parseInt(cInt[1]);
            Sensor interrogador = new Sensor(Sensor.ID_INTERROGADOR,
                    new Ponto(x, y), Sensor.LAMBDA_INTERROGADOR);

            //Quantidade máxima de sensores por canal
            //Adicionamos mais 1 pois o interrogador também é tratado como um
            //sensor e também é instalado na fibra.
            //TODO Isso parece POG.
            int qtdeMaxSensorCanal = Integer.parseInt(prop.getProperty("max_channel_sensor")) + 1;

            //Lê os conjuntos de sensores e regiões.
            ArrayList<Sensor> conjSensores = new ArrayList<>();
            ArrayList<Poligono> conjRegInt = new ArrayList<>();
            ArrayList<Poligono> conjRegPen = new ArrayList<>();

            Enumeration<?> conjChaves = prop.propertyNames();
            while (conjChaves.hasMoreElements()) {
                String chave = (String) conjChaves.nextElement();
                //O nome do elemento (sensor ou região) é sempre logo após o _
                String idStr = chave.substring(chave.lastIndexOf("_") + 1);

                //Sensor
                if (chave.matches("sensor_\\w+")) {
                    //Separa a coordenada do lambda. Ex.: 5,3;400
                    String[] valor = prop.getProperty(chave).split(";");
                    String[] coords = valor[0].split(",");
                    int id = Integer.parseInt(idStr);
                    int sensor_x = Integer.parseInt(coords[0]);
                    int sensor_y = Integer.parseInt(coords[1]);
                    double lambda = Double.parseDouble(valor[1]);
                    //Cria um sensor e adiciona no arraylist.
                    Sensor s = new Sensor(id, new Ponto(sensor_x, sensor_y), lambda);
                    conjSensores.add(s);
                } else if (chave.matches("regiao_int_\\w+") || chave.matches("regiao_pen_\\w+")) { //regiões
                    int id = Integer.parseInt(idStr);
                    //Separa todas coordenadas da região. Ex.: 4,5;8,4;9,2;2,4
                    String[] valor = prop.getProperty(chave).split(";");
                    //Cria a região
                    Poligono regiao = new Poligono(id);
                    //Insere os pontos no polígono (região).
                    for (String v : valor) {
                        String[] coords = v.split(",");
                        int rx = Integer.parseInt(coords[0]);
                        int ry = Integer.parseInt(coords[1]);
                        regiao.adicionarPonto(new Ponto(rx, ry));
                    }
                    //Adiciona a região ao arraylist adequado
                    if (chave.matches("regiao_int_\\w+")) {
                        conjRegInt.add(regiao); //intransponível
                    } else if (chave.matches("regiao_pen_\\w+")) {
                        conjRegPen.add(regiao); //indesejável
                    }
                }
            }

            //Transforma os arraylists em arrays
            //Interrogador + Sensores
            Sensor[] sensores = new Sensor[conjSensores.size() + 1];
            sensores[0] = interrogador;
            for (int i = 1; i < sensores.length; i++) {
                sensores[i] = conjSensores.get(i - 1);
            }
            //Regiões intransponíveis
            Poligono[] regioesIntrans = new Poligono[conjRegInt.size()];
            for (int i = 0; i < regioesIntrans.length; i++) {
                regioesIntrans[i] = conjRegInt.get(i);
            }
            //Regiões com penalidades
            Poligono[] regioesPenalidade = new Poligono[conjRegPen.size()];
            for (int i = 0; i < regioesPenalidade.length; i++) {
                regioesPenalidade[i] = conjRegPen.get(i);
            }

            //Guarda tudo na classe Topologia para uso futuro.
            Topologia.qtdeMaxCanais = qtdeMaxCanais;
            Topologia.qtdeMaxSensorCanal = qtdeMaxSensorCanal;
            Topologia.sensores = sensores;
            Topologia.regioesPenalidade = regioesPenalidade;
            Topologia.regioesIntrans = regioesIntrans;

        } catch (IOException ex) {
            //Se ocorrer um erro na leitura, encerra o programa.
            System.out.println("Erro ao ler arquivo de configuração:\n" + ex);
            System.exit(-1);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    System.out.println("Erro ao ler arquivo de configuração:\n" + e);
                }
            }
        }
    }

}
