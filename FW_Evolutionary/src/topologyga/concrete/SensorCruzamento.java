/*
 * Copyright (C) 2014 Manoel Afonso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package topologyga.concrete;

import java.util.ArrayList;
import topologyga.context.AG;
import topologyga.population.Canal;
import topologyga.population.Individuo;
import topologyga.population.Sensor;
import topologyga.strategy.Cruzamento;
import topologyga.util.Topologia;

/**
 * Implementação do crossover de ordem 1. A ideia é preservar a ordem relativa
 * em que os elementos ocorrem.
 *
 * @author Manoel Afonso
 */
public class SensorCruzamento extends Cruzamento {

    @Override
    public Individuo[] executar(Individuo... ind) {
        //Cromossomo dos pais com as IDs dos sensores.
        double[] pai1 = traduzirGenesParaSensor(ind[0], ind[0].getCromossomo());
        double[] pai2 = traduzirGenesParaSensor(ind[1], ind[1].getCromossomo());

        //Filhos a serem gerados
        Individuo[] filhos = new Individuo[2];
        filhos[0] = new Individuo(Topologia.qtdeMaxCanais, Topologia.qtdeMaxSensorCanal);
        filhos[1] = new Individuo(Topologia.qtdeMaxCanais, Topologia.qtdeMaxSensorCanal);

        //Sorteia dois pontos distintos que delimitarão a região que será mantida.
        int ponto1 = AG.prng.nextInt(pai1.length);
        int ponto2 = AG.prng.nextInt(pai1.length);
        while (ponto1 == ponto2) {
            ponto2 = AG.prng.nextInt(pai1.length);
        }
        //P1 sempre tem que ser menor que P2
        if (ponto1 > ponto2) {
            int temp = ponto2;
            ponto2 = ponto1;
            ponto1 = temp;
        }

        //Cria o cromossomo das IDs que serão geradas para os filhos.
        double[] filho1 = new double[pai1.length];
        double[] filho2 = new double[pai1.length];
        //Inicialmente vazios
        for (int i = 0; i < filho1.length; i++) {
            filho1[i] = Individuo.GENE_INATIVO;
            filho2[i] = Individuo.GENE_INATIVO;
        }

        //Copia os trechos em comum dos pais para os filhos.
        for (int i = ponto1; i < ponto2; i++) {
            filho1[i] = pai1[i];
            filho2[i] = pai2[i];
        }

        //Realiza o crossover. Genes de pais reversos que não estão nos filhos
        //são copiados.
        this.preencherFilho(filho1, pai2, ponto2);
        this.preencherFilho(filho2, pai1, ponto2);

        //Reduz os cromossomos
        this.trimCromossomo(filho1);
        this.trimCromossomo(filho2);

        //Insere o interrogador onde for necessário.
        this.inserirInterrogador(filho1);
        this.inserirInterrogador(filho2);

        //Garante que os interrogadores estejam na primeira posição do canal.
        this.moverInterrgador(filho1);
        this.moverInterrgador(filho2);

        //Insere os cromossomos gerados nos filhos
        filhos[0].setCromossomo(this.traduzirSensorParaGenes(filho1));
        filhos[1].setCromossomo(this.traduzirSensorParaGenes(filho2));

        //Insere os objetos Canal de volta para os filhos.
        Canal[] canaisFilho1 = this.criarCanal(filhos[0].getCromossomo());
        filhos[0].adicionarCanal(canaisFilho1);

        Canal[] canaisFilho2 = this.criarCanal(filhos[1].getCromossomo());
        filhos[1].adicionarCanal(canaisFilho2);

        return filhos;
    }

    private double[] traduzirGenesParaSensor(Individuo ind, double[] cromossomo) {
        double[] traduzido = new double[cromossomo.length];
        for (int i = 0; i < traduzido.length; i++) {
            traduzido[i] = Individuo.GENE_INATIVO;
        }

        Canal[] canais = ind.getCanais();
        int cont = 0;
        for (Canal canal : canais) {
            if (canal != null) {
                Sensor[] sensores = canal.getSensores();
                for (Sensor sensor : sensores) {
                    if (sensor != null) {
                        if (!sensor.isInterrogador()) {
                            traduzido[cont] = sensor.getId();
                        } else {
                            traduzido[cont] = Individuo.GENE_INTERROGADOR;
                        }

                    } else {
                        traduzido[cont] = Individuo.GENE_INATIVO;
                    }
                    cont++;
                }
            } else {
                //Se este canal for nulo, pula para o próximo.
                cont += Topologia.qtdeMaxSensorCanal;
            }
        }

        return traduzido;
    }

    private double[] traduzirSensorParaGenes(double[] cromossomo) {
        //Cria o cromossomo traduzido para os genes de intervalos.
        double[] traduzido = new double[cromossomo.length];
        //Inicialmente vazio.
        for (int i = 0; i < traduzido.length; i++) {
            traduzido[i] = Individuo.GENE_INATIVO;
        }

        int cont = 0;
        for (int i = 0; i < cromossomo.length; i++) {
            //Vamos até o penúltimo gene para poder conseguirmos pegar o próximo
            if (i < cromossomo.length - 1) {
                double datual = cromossomo[i];
                double dproximo = cromossomo[i + 1];

                //Se a próxima ID já é um interrogador deixamos o gene em branco,
                //pois não leva a lugar algum. O mesmo se o próximo já for um
                //gene em branco.
                if (dproximo == Individuo.GENE_INTERROGADOR
                        || dproximo == Individuo.GENE_INATIVO) {
                    cont++;
                    continue;
                }

                int atual = (int) datual;
                int proximo = (int) dproximo;

                Sensor sensorAtual = idParaSensor(atual);
                Sensor sensorProximo = idParaSensor(proximo);

                int indice = indiceDeSensor(sensorAtual, sensorProximo);

                double gene = sensorAtual.indiceParaGene(indice);

                traduzido[cont] = gene;
                cont++;
            }
        }

        return traduzido;
    }

    /**
     * Calcula o índice do sensor {@code l} na lista de adjacências de
     * {@code sensor}.
     *
     * @param sensor Sensor principal
     * @param l Sensor que se deseja saber o índice.
     * @return Índice de {@code l} na lista de adjacências de {@code sensor} ou
     * -1 caso ele não seja encontrado.
     */
    private int indiceDeSensor(Sensor sensor, Sensor l) {
        ArrayList<Sensor> adjacentes = sensor.getAdjacentes();
        return adjacentes.indexOf(l);
    }

    /**
     * Procura o objeto sensor que possua a ID {@code id}.
     *
     * @param id ID a ser procurada.
     * @return Objeto sensor que possui a ID {@code id} ou {@code null} caso a
     * ID não seja encontrada na lista de sensores.
     */
    private Sensor idParaSensor(int id) {
        Sensor[] sensores = Topologia.sensores;

        if (id == Sensor.ID_INTERROGADOR) {
            //O interrogador é sempre o primeiro.
            return sensores[0];
        } else {
            //Procuramos o sensor
            for (Sensor tmp : sensores) {
                if (tmp.isInterrogador()) {
                    continue;
                }

                if (tmp.getId() == id) {
                    return tmp;
                }
            }
        }

        System.out.println("ERRO: ID " + id + " não encontrada.");

        return null;
    }

    /**
     * Move os interrogadores para o início de cada canal caso seja necessário.
     * Exemplo: {@code 6 7 6 1 -1 3 7 5 1 -1}<br> {@code -1 6 7 6 1 -1 3 7 5 1}
     *
     * @param cromossomo
     */
    private void moverInterrgador(double[] cromossomo) {
        int qtdeCanal = Topologia.qtdeMaxCanais;
        int maxSens = Topologia.qtdeMaxSensorCanal;

        int ini = 0;

        //limites de cada canal.
        int p1 = ini;
        int p2 = maxSens;

        //Percorre cada canal
        for (int k = 0; k < qtdeCanal; k++) {
            //Intervalo do canal no cromossomo
            for (int i = p1; i < p2; i++) {
                //Se ao percorremos um canal, encontrarmos um interrogador, fazemos
                //os ajustes.
                if (cromossomo[i] == Individuo.GENE_INTERROGADOR) {
                    //Nova array ajustado
                    double[] novo = new double[maxSens];
                    //Insere o interrogador na primeira posição.
                    novo[0] = Individuo.GENE_INTERROGADOR;
                    //Começamos a inserir no novo a partir da posição 1.
                    int posNovo = 1;
                    //Percorre o canal copiando valores diferentes de -1.
                    for (int j = p1; j < p2; j++) {
                        if (cromossomo[j] != Individuo.GENE_INTERROGADOR) {
                            novo[posNovo] = cromossomo[j];
                            posNovo++;
                        }
                    }
                    //Insere o array ajustado no cromossomo.
                    System.arraycopy(novo, 0, cromossomo, p1, novo.length);
                    break;
                }
            }
            //Intervalo do próximo canal
            p1 += maxSens;
            p2 += maxSens;
        }
    }

    /**
     * Insere um interrogador nos canais que ainda não o tem. É preciso que o
     * cromossomo já esteja reduzido e haja pelo menos uma posição vaga.
     * Exemplo:<br> {@code 6 7 6 2 -0.5 3 7 -0.5 -0.5 -0.5} (Entrada)<br>
     * {@code -1 6 7 6 2 -1 3 7 -0.5 -0.5} (Saída).
     *
     * @param cromossomo
     */
    private void inserirInterrogador(double[] cromossomo) {
        //já há interrogadores em todos os canais
        if (frequencia(cromossomo, Individuo.GENE_INTERROGADOR) == Topologia.qtdeMaxCanais) {
            return;
        }

        int qtdeCanal = Topologia.qtdeMaxCanais;
        int maxSens = Topologia.qtdeMaxSensorCanal;

        boolean[] temInt = new boolean[qtdeCanal];
        int ini = 0;
        int canal = 0;

        //Percorre cada canal
        //limites de cada canal.
        int p1 = ini;
        int p2 = maxSens;
        for (int k = 0; k < qtdeCanal; k++) {
            for (int i = p1; i < p2; i++) {
                if (cromossomo[i] == Individuo.GENE_INTERROGADOR) {
                    //achamos um interrogador neste canal.
                    temInt[canal] = true;
                    break;
                }
            }
            p1 += maxSens;
            p2 += maxSens;
            canal++;
        }

        for (int i = 0; i < temInt.length; i++) {
            if (!temInt[i]) {//canal sem interrogador
                //Começo do canal
                ini = i * maxSens;
                int aux = i * maxSens;//para a cópia de volta.

                //cria um canal com o interrogador.
                double[] novoCanal = new double[maxSens];
                novoCanal[0] = Individuo.GENE_INTERROGADOR;

                for (int j = 1; j < novoCanal.length; j++) {
                    novoCanal[j] = cromossomo[ini];
                    ini++;
                }
                //copia de volta para o cromossomo
                int x = 0;
                for (int j = aux; j < aux + maxSens; j++) {
                    cromossomo[j] = novoCanal[x];
                    x++;
                }
            }
        }
    }

    /**
     * Calcula a quantidade de vezes que {@code val} aparece em
     * {@code array}.
     * @param array Conjunto de valores.
     * @param val Valor a ser pesquisado.
     * @return A frequência de {@code val} em {@code array}.
     */
    private int frequencia(double[] array, double val) {
        int f = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == val) {
                f++;
            }
        }
        return f;
    }

    /**
     * Reduz o cromossomo. Genes inativos são movidos para o fim de um canal.
     * Exemplo com cromossomo de com 2 canais de 5 sensores:<br>
     * [-0.5] [-0.5] <b>[0.7]</b> [-0.5] <b>[0.3]</b> [-0.5] <b>[0.1]</b>
     * <b>[0.7]</b> [-0.5] <b>[0.8]</b><br>
     * Após a redução:<br>
     * <b>[0.7] [0.3]</b> [-0.5] [-0.5] [-0.5] <b>[0.1] [0.7] [0.8]</b> [-0.5]
     * [-0.5]
     *
     * @param cromossomo Cromossomo a ser reduzido.
     */
    private void trimCromossomo(double[] cromossomo) {
        int qtdeCanal = Topologia.qtdeMaxCanais;
        int maxSens = Topologia.qtdeMaxSensorCanal;
        //Conjunto de todos os canais reduzidos
        double[][] canaisTrimmed = new double[qtdeCanal][maxSens];
        //Inicia os canais com genes inativos
        for (double[] canai : canaisTrimmed) {
            for (int i = 0; i < canai.length; i++) {
                canai[i] = Individuo.GENE_INATIVO;
            }
        }

        //Índice inicial de cada canal
        int indIniCanal = 0;
        //Para cada canal faz o trim
        for (double[] canai : canaisTrimmed) {
            //Índice vago no canal reduzido
            int indVago = 0;
            //Para os genes deste canal, procura os genes ativos
            for (int j = indIniCanal; j < indIniCanal + maxSens; j++) {
                if (cromossomo[j] != Individuo.GENE_INATIVO) {
                    //Encontrou um gene ativo. Incrementa o índice vago.
                    canai[indVago] = cromossomo[j];
                    indVago++;
                }
            }
            //Passa para o próximo canal.
            indIniCanal += maxSens;
        }

        //Copia os canais reduzidos para o cromossomo.
        indIniCanal = 0;
        for (double[] canai : canaisTrimmed) {
            System.arraycopy(canai, 0, cromossomo, indIniCanal, canai.length);
            //Passa para o próximo canal.
            indIniCanal += maxSens;
        }

    }

    private Canal[] criarCanal(double[] cromossomo) {
        //Interrogador da topologia.
        Sensor interrogador = Topologia.sensores[0];

        //Cria os canais disponíveis.
        Canal[] canais = new Canal[Topologia.qtdeMaxCanais];

        //Índice que representa do início de um canal no cromossomo. Começa em 0
        //e depois vai aumentando de acordo com o tamanho dos canais.
        int indIniCanal = 0;

        //Preenche os canais com os sensores de acordo com os genes.
        for (int i = 0; i < canais.length; i++) {
            //Cria um canal.
            canais[i] = new Canal(Topologia.qtdeMaxSensorCanal);
            canais[i].setID(i);

            //Próximo sensor apontado pelo gene.
            Sensor prox;

            //Só continua as inserções caso os genes estiverem ativos.
            if (cromossomo[indIniCanal] != Individuo.GENE_INATIVO
                    && cromossomo[indIniCanal] != Individuo.GENE_INTERROGADOR) {
                //Insere o interrogador no canal atual com o primeiro cromossomo,
                //que aponta para o próximo sensor.
                canais[i].addSensor(interrogador, cromossomo[indIniCanal]);
                //Obtém o índice do próximo sensor na lista de adjacências.
                int indProx = interrogador.geneParaIndice(cromossomo[indIniCanal]);
                //Armazena o próximo sensor.
                prox = interrogador.getAdjacentes().get(indProx);
            } else {
                //Como o primeiro gene está desativado, então não há sensores
                //neste canal. Vamos para o próximo canal.
                //Índice que marca o início do próximo canal.
                indIniCanal += Topologia.qtdeMaxSensorCanal;
                continue;
            }

            //Vai adicionando quantos sensores houver neste canal.
            //Vamos até o limite máximo deste canal.
            for (int j = indIniCanal + 1; j < indIniCanal + Topologia.qtdeMaxSensorCanal; j++) {
                //Adiciona o próximo sensor mais o gene para o próximo sensor.
                canais[i].addSensor(prox, cromossomo[j]);

                //Se o gene atual estiver ativo.
                if (cromossomo[j] != Individuo.GENE_INATIVO) {
                    //Descobre o próximo sensor.
                    int indProx = prox.geneParaIndice(cromossomo[j]);
                    prox = prox.getAdjacentes().get(indProx);
                } else {
                    break;
                }
            }

            //Índice que marca o início do próximo canal.
            indIniCanal += Topologia.qtdeMaxSensorCanal;
        }

        return canais;
    }

    /**
     * A partir do {@code ponto2}, os genes do {@code pai} são copiados para o
     * {@code filho} desde que não haja repetições. A cópia é circular e termina
     * quando se atinge {@code ponto2} novamente.
     *
     * @param filho Filho a ter os genes preenchidos.
     * @param pai Pai de onde os genes serão copiados.
     * @param ponto1 Ponto esquerdo da região que não é alterada.
     * @param ponto2 Ponto direito da região que não é alterada.
     */
    private void preencherFilho(double[] filho, double[] pai, int ponto2) {
        //Posição que falta ser copiada para o filho.
        int posCop = ponto2;
        int volta = 0;
        for (int i = 0; i < filho.length; i++) {
            //Posição atual
            int pos = (i + ponto2) % filho.length;

            //Verifica se já terminou de dar a volta.
            if (pos == ponto2) {
                volta++;
                if (volta == 2) {
                    break;
                }
            }

            //Faz as cópias nas posições que faltam.
            if (!this.arrayContem(filho, pai[pos])) {
                filho[posCop] = pai[pos];
                //Atualiza a próxima posição que deverá receber um valor.
                posCop = (posCop + 1) % filho.length;
            }
        }
    }

    /**
     * Verifica se o array {@code arr} possui o valor {@code val}.
     *
     * @param arr Array.
     * @param val Valor a ser verificado.
     * @return true se {@code val} está presente em {@code arr}.
     */
    private boolean arrayContem(double[] arr, double val) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == val) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "Probabilidade de cruzamento: " + this.getTaxaCruzamento();
    }

}
