/*
 * Copyright (C) 2014 Manoel Afonso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package topologyga.concrete;

import java.util.ArrayList;
import java.util.Collections;
import topologyga.population.Individuo;
import topologyga.population.Populacao;
import topologyga.strategy.Replacement;

/**
 * Implementação da seleção de sobreviventes. A seleção consiste em substituir
 * os piores indivíduos da população atual pelos indivíduos da offspring.
 *
 * @author Manoel Afonso
 */
public class SensorReplacement implements Replacement {

    @Override
    public void executar(Populacao original, Populacao offspring) {
        ArrayList<Individuo> indOri = original.getIndividuos();
        //Ordena em ordem crescente de fitness
        Collections.sort(indOri);
        //Substitui os piores pela offspring.
        for (int i = 0; i < offspring.getTamanhoPop(); i++) {
            indOri.set(i, offspring.getIndividuoAt(i));
        }
    }

}
