/*
 * Copyright (C) 2014 Manoel Afonso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package topologyga.strategy;

import topologyga.population.Populacao;

/**
 * Implementação do seleção dos sobreviventes.
 *
 * @author Manoel Afonso
 */
public interface Replacement {

    /**
     * Cria a população que irá para a próxima geração de acordo com um
     * critério.
     *
     * @param original População da geração atual.
     * @param offspring Descendentes da população atual que, por algum critério,
     * serão inseridos na nova população.
     */
    public void executar(Populacao original, Populacao offspring);

}
