/*
 * Copyright (C) 2014 Manoel Afonso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package topologyga.strategy;

import topologyga.population.Individuo;

/**
 * Interface para o cálculo do fitness de um indivíduo.
 *
 * @author Manoel Afonso
 */
public interface Fitness {

    /**
     * Calcula o fitness do indivíduo.
     *
     * @param ind Indivíduo a ser analisado.
     * @return Um valor no intervalo [0,1] representando o fitness do indivíduo.
     */
    public double calcular(Individuo ind);

}
