/*
 * Copyright (C) 2014 Manoel Afonso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package topologyga.strategy;

import topologyga.population.Individuo;

/**
 * Especificação do operador de dominação. Ele determina se um indivíduo
 * domina outro com base nos objetivos. Como há várias combinações para a
 * otimização dos objetivos (min x min, min x max, etc), cada AG multiobjetivo
 * deve implementar a sua forma específica de avaliar a relação de dominância.
 * Note que um indivíduo {@code A} domina o indivíduo {@code B} outro se
 * {@code A} vence {@code B} em <b>todos</b> os objetivos.
 * @author Manoel Afonso
 */
public interface Dominacao {
    
    /**
     * Verifica a relação de dominância entre os indivíduos {@code ind1}
     * e {@code ind2}.
     * @param ind1 Indivíduo 1.
     * @param ind2 Indivíduo 2.
     * @return true se {@code ind1} domina o {@code ind2}, false caso ele não
     * o domine.
     */
    public boolean dominates(Individuo ind1, Individuo ind2);
    
}
