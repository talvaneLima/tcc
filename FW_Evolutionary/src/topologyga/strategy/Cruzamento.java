/*
 * Copyright (C) 2014 Manoel Afonso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package topologyga.strategy;

import topologyga.population.Individuo;

/**
 * Especificação do operador Cruzamento.
 *
 * @author Manoel Afonso
 */
public abstract class Cruzamento {

    private double taxaCruzamento = 0;

    /**
     * Realiza o crossover entre indivíduos.
     *
     * @param ind Indivíduos a participarem do crossover.
     * @return Descendentes.
     */
    public abstract Individuo[] executar(Individuo... ind);

    /**
     * @return A taxa de cruzamento.
     */
    public double getTaxaCruzamento() {
        return taxaCruzamento;
    }

    /**
     * Configura a taxa de cruzamento.
     *
     * @param taxaCruzamento Valor entre [0, 1].
     */
    public void setTaxaCruzamento(double taxaCruzamento) {
        this.taxaCruzamento = taxaCruzamento;
    }

}
