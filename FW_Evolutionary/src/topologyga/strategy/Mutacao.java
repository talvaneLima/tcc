/*
 * Copyright (C) 2014 Manoel Afonso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package topologyga.strategy;

import topologyga.population.Individuo;

/**
 * Especificação do operador de Mutação.
 *
 * @author Manoel Afonso
 */
public abstract class Mutacao {

    /**
     * Taxa de mutação.
     */
    private double taxaMutacao = 0;

    /**
     * Realiza a mutação no indivíduo.
     *
     * @param ind Indivíduo alvo.
     * @return Novo indivíduo.
     */
    public abstract Individuo executar(Individuo ind);

    /**
     * @return A taxa de mutação.
     */
    public double getTaxaMutacao() {
        return taxaMutacao;
    }

    /**
     * Configura a taxa de mutação.
     *
     * @param tm Valor entre [0, 1].
     */
    public void setTaxaMutacao(double tm) {
        this.taxaMutacao = tm;
    }

}
