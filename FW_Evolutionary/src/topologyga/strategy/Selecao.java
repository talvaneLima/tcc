/*
 * Copyright (C) 2014 Manoel Afonso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package topologyga.strategy;

import topologyga.population.Populacao;

/**
 * Especificação do operador de Seleção do AG.
 *
 * @author Manoel Afonso
 */
public abstract class Selecao {

    /**
     * Total de indivíduos a serem selecionados.
     */
    private int totalIndividuos;

    /**
     * Proporção (em %) do total de indivíduos a serem selecionados da
     * população.
     */
    private double proporcao = 0;

    /**
     * Executa a seleção de indivíduos para o crossover e mutação.
     *
     * @param pop População alvo.
     * @return População com os indivíduos selecionados.
     */
    public abstract Populacao executar(Populacao pop);

    /**
     * @return the proporcao
     */
    public double getProporcao() {
        return proporcao;
    }

    /**
     * Configura a proporção de indivíduos a ser selecionada. É importante que
     * {@link Populacao#numIndividuos} já esteja configurado pois a quantidade
     * de indivíduos que será selecionada também é calculada.
     *
     * @param proporcao the proporcao to set
     */
    public void setProporcao(double proporcao) {
        this.proporcao = proporcao;
        this.totalIndividuos = (int) (Populacao.numIndividuos * this.proporcao);
    }

    /**
     * @return O total de indivíduos a serem selecionados.
     */
    public int getTotalIndividuos() {
        return totalIndividuos;
    }

}
