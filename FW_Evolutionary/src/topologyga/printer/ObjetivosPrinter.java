/*
 * Copyright (C) 2014 Manoel Afonso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package topologyga.printer;

import topologyga.population.Individuo;

/**
 * Imprime os objetivos e a frente de pareto.
 * @author Manoel Afonso
 */
public class ObjetivosPrinter implements Printer {

    @Override
    public String print(Individuo ind) {
        double[] objetivos = ind.getObjetivos();
        
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < objetivos.length; i++) {
            result.append(objetivos[i]);
            result.append("\t");
        }
        result.append(ind.getParetoFront());
        
        return result.toString();
    }
    
}
