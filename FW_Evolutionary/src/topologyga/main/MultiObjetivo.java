/*
 * Copyright (C) 2014 Manoel Afonso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package topologyga.main;

import topologyga.concrete.NSGAIIDominacao;
import topologyga.concrete.NSGAIIReplacement;
import topologyga.concrete.SensorCruzamento;
import topologyga.concrete.SensorInicializacao;
import topologyga.concrete.SensorMutacao;
import topologyga.concrete.SensorSelecao;
import topologyga.context.AG;
import topologyga.context.NSGAIIAG;
import topologyga.fitness.NSGAIIFitness;
import topologyga.fitness.NSGAIIObjetivos;
import topologyga.population.Individuo;
import topologyga.printer.DecodificacaoPrinter;
import topologyga.printer.ObjetivosPrinter;
import topologyga.printer.TopologyPrinter;
import topologyga.strategy.Cruzamento;
import topologyga.strategy.Dominacao;
import topologyga.strategy.Fitness;
import topologyga.strategy.Inicializacao;
import topologyga.strategy.Mutacao;
import topologyga.strategy.Objetivo;
import topologyga.strategy.Replacement;
import topologyga.strategy.Selecao;

/**
 * Classe principal para a execução do AG multiobjetivo.
 *
 * @author Manoel Afonso
 */
public class MultiObjetivo {

    public static void main(String[] args) {
        //Instancia os operadores
        Inicializacao ini = new SensorInicializacao("ag_map.properties");
        Fitness fit = new NSGAIIFitness();
        Selecao sel = new SensorSelecao();
        Cruzamento cruz = new SensorCruzamento();
        Mutacao mut = new SensorMutacao();
        Replacement repla = new NSGAIIReplacement();
        //Operadores específicos do AG multiobjetivo.
        Objetivo objetivos = new NSGAIIObjetivos();
        Dominacao dom = new NSGAIIDominacao();
        
        //Printer para os indivíduos. Como eles devem ser impressos.
        Individuo.printer = new ObjetivosPrinter();

        //Os insere no AG
        AG ag = new NSGAIIAG(ini, sel, cruz, mut, repla, fit, objetivos, dom);
        ag.carregarConfig();

        //Número de execuções do AG
        int numExc = 1;
        
        for (int i = 0; i < numExc; i++) {
            executarAG(ag);
            ag.reset();
        }
    }

    public static void executarAG(AG ag) {
        //Inicialização
        ag.gerarPopulacaoInicial();
        
        //Imprime a 1ª geração.
        System.out.println(ag.getPopulacao());
        
        //Executa os operadores durante o número de gerações.
        for (int i = 0; i < AG.getNumGeracoes(); i++) {
            ag.executarGeracao();
            System.out.println("GERAÇÃO "+(i+1));
            System.out.println(ag.getPopulacao());
        }

        /*
        Imprime a população final da versão multiobjetivo.
         */
        System.out.println("GERAÇÃO 500");
        System.out.println(ag.getPopulacao());
        Individuo.printer = new DecodificacaoPrinter();
        System.out.println(ag.getPopulacao());
        Individuo.printer = new TopologyPrinter();
        System.out.println(ag.getPopulacao());
    }

}
