/*
 * Copyright (C) 2014 Manoel Afonso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package topologyga.main;

import topologyga.concrete.SensorCruzamento;
import topologyga.concrete.SensorInicializacao;
import topologyga.concrete.SensorMutacao;
import topologyga.concrete.SensorReplacement;
import topologyga.concrete.SensorSelecao;
import topologyga.context.AG;
import topologyga.context.TopologyAG;
import topologyga.fitness.SensorFitness;
import topologyga.population.Individuo;
import topologyga.printer.TopologyPrinter;
import topologyga.strategy.Cruzamento;
import topologyga.strategy.Fitness;
import topologyga.strategy.Inicializacao;
import topologyga.strategy.Mutacao;
import topologyga.strategy.Replacement;
import topologyga.strategy.Selecao;

/**
 * Classe principal para a execução do AG mono-objetivo.
 *
 * @author Manoel Afonso
 */
public class MonoObjetivo {

    public static void main(String[] args) {
        //Instancia os operadores
        Inicializacao ini = new SensorInicializacao("ag_map.properties");
        Fitness fit = new SensorFitness();
        Selecao sel = new SensorSelecao();
        Cruzamento cruz = new SensorCruzamento();
        Mutacao mut = new SensorMutacao();
        Replacement repla = new SensorReplacement();
        
        Individuo.printer = new TopologyPrinter();

        //Os insere no AG
        AG ag = new TopologyAG(ini, sel, cruz, mut, repla, fit);
        ag.carregarConfig();

        //Número de execuções do AG
        int numExc = 1;

        for (int i = 0; i < numExc; i++) {
            executarAG(ag);
            ag.reset();
        }
    }

    public static void executarAG(AG ag) {
        //Inicialização
        ag.gerarPopulacaoInicial();

        //Executa os operadores durante o número de gerações.
        for (int i = 0; i < AG.getNumGeracoes(); i++) {
            ag.executarGeracao();
            //Relatório para AG elitista
            //System.out.println(ag.getMelhorSolucao().getFitness()+"\t"+ag.getMelhorSolucao().getComprimentoTotal());
        }

        //Melhor solução final
        System.out.println(ag.getMelhorSolucao());
    }

}
