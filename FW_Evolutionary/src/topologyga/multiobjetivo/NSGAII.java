package topologyga.multiobjetivo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import topologyga.compator.FactoryComparator;
import topologyga.compator.ParetoFrontComparator;
import topologyga.population.Individuo;
import topologyga.strategy.Dominacao;
import topologyga.util.Constants;

/**
 * Implementação do algoritmo NSGAII.
 *
 * @author Manoel Afonso
 * @author Reginaldo Filho
 */
public class NSGAII {

    /**
     * Calcula a frente de pareto de todos os indivíduos. Essa informação é
     * utilizada para calcular o fitness de todos eles. Este método tem uma
     * complexidade geral de O(MN²), onde M é o número de objetivos e N é o
     * tamanho da população.
     *
     * @param inds Indivíduos da população.
     * @param dom Operador de dominação.
     */
    public static void fastNonDominatedSort(Individuo inds[], Dominacao dom) {
        Domination S[] = Domination.createDomination(inds.length);
        ArrayList<Integer> F = new ArrayList<>();

        for (int p = 0; p < inds.length; p++) {
            for (int q = 0; q < inds.length; q++) {
                if (p == q) {
                    continue;
                }
                if (dom.dominates(inds[p], inds[q])) {
                    S[p].dominated.add(q); // sol. p dominates sol. q
                } else if (dom.dominates(inds[q], inds[p])) {
                    S[p].dominationCount++; // sol. q dominates sol. p
                }
            }
            if (S[p].dominationCount == 0) {
                inds[p].setParetoFront(1);
                F.add(p);
            }
        }

        int i = 1; // i is the front counter
        while (!F.isEmpty()) {
            ArrayList<Integer> Q = new ArrayList<>();
            for (Integer p : F) {
                for (Integer q : S[p].dominated) {
                    S[q].dominationCount--;
                    if (S[q].dominationCount == 0) {
                        inds[q].setParetoFront(i + 1);
                        Q.add(q);
                    }
                }
            }
            i++;
            F = Q;
        }
    }

    public static void calculateCrowdingDistances(Individuo chromosomes[]) {
        for (Individuo chromosome : chromosomes) {
            chromosome.setCrowdingDistance(0);
        }

        Collections.sort(Arrays.asList(chromosomes), new ParetoFrontComparator());
        int firstParetoFront = chromosomes[0].getParetoFront();
        int lastParetoFront = chromosomes[chromosomes.length - 1].getParetoFront();
        int firstPos = 0, size;

        for (int i = firstParetoFront; i <= lastParetoFront; i++) {

            size = getParetoFrontSize(chromosomes, firstPos, i);
            Individuo I[] = new Individuo[size];
            System.arraycopy(chromosomes, firstPos, I, 0, I.length);

            if (size > 1) {
                for (int obj = 0; obj < Constants.NOBJECTIVES; obj++) {
                    // Sort by objective here
                    Collections.sort(Arrays.asList(I), FactoryComparator.getComparator(obj));

                    I[0].setCrowdingDistance(Double.POSITIVE_INFINITY);
                    I[I.length - 1].setCrowdingDistance(Double.POSITIVE_INFINITY);

                    for (int j = 1; j < I.length - 1; j++) {
                        I[j].setCrowdingDistance(I[j].getCrowdingDistance() + I[j + 1].getObjective(obj) - I[j - 1].getObjective(obj));
                    }
                }

            } else {
                I[0].setCrowdingDistance(Double.POSITIVE_INFINITY);
            }

            firstPos += size;
        }
    }

    public static int getParetoFrontSize(Individuo chromosomes[], int firstPos, int paretoFront) {
        int count = 0;
        for (int i = firstPos; i < chromosomes.length; i++) {
            if (chromosomes[i].getParetoFront() != paretoFront) {
                break;
            }
            count++;
        }
        return count;
    }

    private static class Domination {

        public int dominationCount;
        public ArrayList<Integer> dominated = new ArrayList<>();

        public static Domination[] createDomination(int size) {
            Domination d[] = new Domination[size];
            for (int i = 0; i < size; i++) {
                d[i] = new Domination();
            }
            return d;
        }
    }
}
